// Declare the main app object
const App = {
    // Properties
    name: "GlobalOne (GO) Restauraunt System",
    version: "2.0.0",
    author: "Francis Villarba <francis.villarba@student.curtin.edu.au>",

    // Time Configuration (When is lunch and dinner?)
    gmt: 8,             // GMT +8 is +ve 8, GMT-8 is -8
    lunchTime: 1100,        // 11 AM
    dinnerTime: 1700,       // 5 PM
    closeTime: 2000,        // 8 PM
    isHoliday: false,   // currently public holiday?
    isClosed: false,    // is currently closed?

};

// Export the App Object
export { App };