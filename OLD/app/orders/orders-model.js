'use strict';

/**
 * Mongoose Model for the MongoDB Orders Collection, for use with GRDE3014
 * Web Authoring Design, Assignment 3.
 * 
 * This file has been forked from orders.js model in GRDE3014, Web Authoring
 * Design, Assignment 1 with various improvements added to better support
 * the expanded requirements in Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.0.0
 * 
 * @requires MongoDB
 * @requires Mongoose
 */

// Setup --------------------------------------------------------------------- /
const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

// Model --------------------------------------------------------------------- /

/**
 * Order Item Note Sub-Document for the Orders Model
 * Used to store and represent customisations, notes and other important
 * information about an item that is ordered by the customer.
 */
const OrderItemNote = new Schema({
    // What type of note is it?
    // Staff Discount, Discount, Promotion, Refund, Exchange, Customisation, Request, Future Use, Future Use
    flag: {
        type: Number,   // Valid numbers 1-9
        required: true
    },
    // Informaation about the change
    comment: {
        type: String,
        required: false
    },
    // Data related to this change, json preferred here, required as we need to know what to do
    data: {
        type: Object,
        required: true
    }
},
{
    timestamps: true,
    versionKey: false
});

/**
 * Order Item Sub-Document for the Orders Model
 * Used to store and represent items that has been ordered by the customer.
 */
const OrderItem = new Schema({
    // What is the item (corresponding to the items db collection)
    item: {
        type: Schema.Types.ObjectId,
        required: true
    },
    // How much are we ordering?
    qty: {
        type: Number,
        required: false,
        default: 1
    },
    // Any notes associated with this order item?
    notes: [
        OrderItemNote
    ],
    // What is the target for the item? Where is it going to?
    // Dine In, Take Away, Delivery, Scheduled DI, Scheduled TA, Scheduled DEL, Future Use x3
    type: { // By default, 1 ==> Dine In
        type: Number,
        required: false,
        default: 1
    } // Type is ignored if we don't have mixed order target boolean == true
},
{
    timestamps: true,
    versionKey: false
});

/**
 * Payment Information Sub-Document for the Orders Model
 * Used to store and represent payment information for the customer's order for tracking etc.
 */
const PaymentInfo = new Schema({
    // What is the payment method?
    method: {
        type: Number,
        required: false,        // False initially for dine-in, guest orders done by waiters for customers
        default: 0,             // Default to not paid 
        enum: [0,1,2,3,4,5,6,7,8,9] // Restrict the numbers allowed here
    },
    // The total price of the order, after calculations, customisations and notes
    totalPrice: {   // Default to 0.00 as calculation has not been done yet
        type: Schema.Types.Decimal128,  // Floating point
        default: 0.00
        // TODO - Future, add a function for calculating this before save...?
    },
    // Timestamp for when this has been processed
    processedAt: {
        type: Date,     // For tracking when orders have been paid for reconciliation
        default: null
    },
    // Who processed the order / payment?
    processor: {
        // This should correspond to staff's _id in the users collection
        type: Schema.Types.ObjectId
    }
},
{
    timestamps: true,
    versionKey: false    
});

/**
 * The orders schema model that correspond to orders documents in the orders
 * collection of the mongoDB database.
 * 
 * Adapted from GRDE3014, Web Authoring Design Assignment 1 with improvements
 * to better handle the requirements of Assignment 3.
 */
const OrdersSchema = new Schema({
    // Who placed the order?
    user: {
        type: Schema.Types.ObjectId,    // _id of the user in the users collection
        required: true                  // Must be added to identify the user
    },
    // What type of order is this?
    // Dine In, Take Away, Delivery, Scheduled DI, Scheduled TA, Scheduled DEL, Future Use X3
    type: {
        type: Number,       // Default to Dine-In
        required: false,    // If not given, will default to dine-in
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9],      // Forces specific values only
        default: 1
    },
    // Determines if the order is of mixed type, this will result in front-end
    // Needing to check each item's individual order type
    isMixedType: {
        type: Number,
        required: false,    // Not required as we have default to dine-in
        default: false      // Default to false
    },
    // If it is a scheduled order, we can input the target time for the order to
    // be completed by. If not suppied, default to Date.now + 10 mins
    schedule: {
        type: Date,         // Date in ISO UTC Format YYYY-mm-ddTHH:MM:ss
        required: false,    // Not required as not all orders are scheduled
        default: Date.now() + 600000
    },
    // The order's current status, for tracking the order's progress + front end filter
    // Unpaid, In Progress, Ready, Completed, Problem, Future Use 5-9
    status: {
        type: Number,
        required: false,
        enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],   // Forces specific values only
        default: 0
    },
    // Has the customer been contacted by the SMS system that their food is ready?
    isNotified: {
        type: Boolean,
        required: false,    // False to reduce size of JSON
        default: false      // Default to false for new orders
    },
    // For front end - Is the order complete? If so, front-end ignore values for display
    isCompleted: {
        type: Boolean,
        required: false,    // Self explanatory, new orders == not done
        default: false
    },
    // The items ordered by the customer
    items: [ // Array of OrderItem Sub-Documents
        OrderItem
    ],
    // Payment information for the order
    payment: PaymentInfo,
    // Has the order been paid for?
    isPaid: {
        type: Boolean,
        required: false,
        default: false
    }
},
{
    timestamps: true,
    versionKey: false
});

// Exports ------------------------------------------------------------------- /

// Primary Orders-Model Export
module.exports = mongoose.model('OrderModel', ordersSchema);   // The OrderModel Schema

// Secondary Exports to aid in Object Creation / Decoding for the Front-end
module.exports = mongoose.model('PaymentInfo', paymentInfo);   // Payment Model
module.exports = mongoose.model('OrderItem', orderItem); // Order Items Model
module.exports = mongoose.model('OrderItemNote', orderItemNote); // Item Note Model