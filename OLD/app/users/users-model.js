'use strict';

/**
 * Mongoose Model for the MongoDB Users Collection, for use with GRDE3014, Web
 * Authoring Design, Assignment 3.
 * 
 * This file has been forked from users.js model in GRDE3014, Web Authoring
 * Design, Assignment 1 with various improvements added to better support
 * the expanded requirements in Assignment 3
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.0.0
 * 
 * @requires MongoDB
 * @requires Mongoose
 * @requires Validator.js
 */

// Setup -------------------------------------------------------------------- /
const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

// Validation
require('mongoose-type-email');         // Add the email mongoose schema type
import validator from 'validator';      // Import Validator Library

// Encryption
bcrypt = require('bcrypt');             // The encryption library
const SALT_WORK_FACTOR = 10;            // The cryptography salting factor
// Set to the default 10 to prevent timeout errors with low-cost servers / hosts

// Model -------------------------------------------------------------------- /
const UsersSchema = new Schema({
    // The full name of the user
    name: {
        type: String,
        required: true,         // We need to know the name of the user
        unique: false           // Not uncommon to have people with the same names therefore false unique requirement
    },
    // Email Address of the User
    email: {
        type: mongoose.SchemaTypes.email,   // Uses mongoose-type-email package
        lowercase: true,        // Ensure the data is stored in lowercase
        trim: true,             // Ensure white spaces before and after are trimmed off
        unique: true,           // Email address must be unique to prevent multiple accounts from same email
        required: true          // Email is required for authentication
    },
    // Password for the account
    password: {
        type: String,       // The password will be encrypted with bcrypt hash/salt
        required: true
    },
    // Has the email address been validated?
    isValidated: {
        type: Boolean,
        default: false
    },
    // Mobile phone for the user
    mobile: {   // TODO - Ensure that this is only mobile numbers in Australia
        type: String,
        required: false,
        trim: true,
        // Custom validator using validator.js module
        validate: {
            validator: (value) => {
                return new Promise((resolve) => {
                    resolve(validator.isMobilePhone(value, "en-AU"));
                });
            },
            message: '`{VALUE}` is not a valid Australian Mobile Phone Number'
        }
    },
    // Does the customer wish to recieve marketing emails?
    marketingEnabled: {
        type: Boolean,          // False by default
        default: false
    },
    // What privileges is assigned to this user?
    privileges: {
        type: [Number],
        enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        // NOTE - You will need to do duplicate prevention here by doing $AddToSet when updating privileges
    },
    // When did the user last login?
    lastLogin: {
        type: Date,         // Default today's date if it is created initially
        default: Date
    },
    // When did the customer last order?
    lastOrder: {
        type: Date,
        default: null      // Default to null initially
    },
    // The customer's order history, based on _id of order in orders colection.
    orderHistory: [
        Schema.Types.ObjectId
    ]
},
{
    timestamps: true,
    versionKey: false
});

// Middleware Functions ----------------------------------------------------- /

/**
 * Encrypt the user's password before we save the user to the database
 * 
 * Adapted from the solution made in GRDE3014, Web Authoring Design,
 * Assignment 1, but improved to use async / promises to increase performance
 * and reliability.
 */
UsersSchema.pre( 'save', (next) => {
    let currentUser = this;

    // If we didn't change the password, execute the callback function
    if( !currentUser.isModified( 'password' )) { return next(); }

    // Declare the encryption promise
    let encryption = new Promise( (resolve, reject) => {
        // Generate a salt, for us to use with hashing
        bcrypt.genSalt( SALT_WORK_FACTOR, (err, salt) => {
            // If there was an error, throw a promise rejection with the error
            if( err ) { reject( err ); }
            // Generate the hash representation of the password
            bcrypt.hash( currentUser.password, salt, (err, hash) => {
                // If there was an error, throw a promise rejection with the error
                if( err ) { reject( err ); }
                // Resolve the promise with a hash
                resolve( hash );
            });
        });
    });

    // Execute the promise
    encryption.then( (hash) => {
        // If resolved without rejection
        currentUser.password = hash;
        next();
    })
    .catch( (err) => {
        // Catch any errors / rejections
        return next(err);
    });
});

/**
 * Encrypt the user's password before we update the user in the database
 * 
 * This is to work around the issue in GRDE3014, Web Authoring Design,
 * Assignment 1, where updating the user's password after the fact using
 * edit user URIs will result in passwords being stored in plain text.
 * 
 * Apparently, on line 1740 of Mongoose Source Code "Model.js", states
 * that findByIdAndUpdate is equivalent to findOneAndUpdate as it is
 * a wrapper, hence the use of findOneAndUpdate for our pre-hook.  
 */
UsersSchema.pre( 'findOneAndUpdate', (next) => {
    // Check if the password is going to be modified in this call
        // If updated, encrypt it
        // If not updated, leave it alone
    // Call next

    // Check if the password is going to be modified
    // TODO - Reference this code
    // Inspired by https://github.com/Automattic/mongoose/issues/4575#issuecomment-321990381

    // Get the password field
    let password = this.getUpdate().$set.password;
    // If the field does not exist, that means we aren't updating it
    if( !password ) { return next(); } // Call the next part of the update chain

    // Update the password
    // Declare the encryption promise
    let encryption = new Promise((resolve, reject) => {
        // Generate a salt, for us to use with hashing
        bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
            // If there was an error, throw a promise rejection with the error
            if (err) {
                reject(err);
            }
            // Generate the hash representation of the password
            bcrypt.hash( password, salt, (err, hash) => {
                // If there was an error, throw a promise rejection with the error
                if (err) {
                    reject(err);
                }
                // Resolve the promise with a hash
                resolve(hash);
            });
        });
    });

    // Run the promise
    encryption.then( (hash) => {
        // If encryption was successful, update the query
        this.getUpdate().$set.password = hash;
        next();
    })
    .catch( (err) => {
        // If an error occured / promise rejection, return it to the callstack
        return next(err);
    });
});

// Instance Methods --------------------------------------------------------- /

/**
 * Verify the user's password against the one stored in the database by
 * comparing the hashed version of the input with the user's password field.
 * 
 * ES6 Arrow functions are not used for this method declaration as it 
 * can cause problems with binding and callbacks in Mongoose. See
 * https: //mongoosejs.com/docs/guide.html#methods for more info.
 * 
 * @module users-model
 * @function
 * @param {String} input - The password for us to check
 * @param {Object} callback - The callback
 * @return {Boolean} isMatched - True if match, false otherwise
 */
UsersSchema.methods.comparePassword = function( input, callback ) {
    // Using bcrypt to comapre the input with the stored password
    bcrypt.compare(input, this.password, function (err, isMatched) {
        if (err) {
            // If an error occurs, throw error up the callback chain
            return callback(err);
        } else {
            // If successful, return no error, but do return boolean result
            callback(null, isMatched);
        }
    });
};

// Exports ------------------------------------------------------------------ /
module.exports = mongoose.model('UserModel', UsersSchema );