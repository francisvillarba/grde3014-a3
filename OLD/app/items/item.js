'use strict';

/**
 * item.js
 * 
 * Class declaration including creation and execution methods for menu items.
 * To be used mainly for the front-end as it calls methods from the backend.
 * 
 * Given the front-end nature of this object, it will not have imports to
 * the server files such as the items-model, items-routes and items-controller
 * and will instead fetch from api calls in an async manner.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 * 
 * @requires NodeJS
 * @requires MongoDB
 * @requires Mongoose
 */

// Front-end Imports -------------------------------------------------------- /

import App from '../app.js';
// TODO Implement Front-End Imports

// Class Declaration -------------------------------------------------------- /

/**
 * Defines the item object itself and all of it's various fields and methods
 */
class Item {
    /**
     * The constructor for the item object
     * @param {String} name - The name of the item
     * @param {String[]} alternateName - The alternative names of the item
     * @param {Object<Mode,Number>} price - The price of the item at different modes, i.e. lunch, dinner etc.
     * @param {String} description - The description of the item
     * @param {Object<Mode,Boolean>} availability - When is the item available
     * @param {Object<Customisation,Object>} customisations - What customisations are supported for the item
     * @param {Boolean} isOnSpecial - Is the item currently on special?
     * @param {Boolean} isAvailable - Is the item currently available?
     * @param {Number} qty - How much of the item remains?
     * @param {Boolean} isInfinite - Is there an infinite number of items?
     * @param {String[]} tags - What tags does the item have?
     * @param {String[]} ingredients - What ingredients does the item have?
     * @param {String} image - The full resolution picture of the item
     * @param {String} thumbnail - The thumbnail resolution picture of the item
     */
    constructor( name, alternateName, price, description, availability, 
        customisations, isOnSpecial, isAvailable, qty, isInfinite, tags, 
        ingredients, image, thumbnail ) {
        this.name;
        this.alternateName;
        this.price;
        this.description;
        this.availability;
        this.customisations;
        this.isOnSpecial;
        this.isAvailable;
        this.qty;
        this.isInfinite;
        this.tags;
        this.ingredients;
    }
    
    /**
     * Is the item currently available for purchase?
     * @returns {Boolean} - True if available, false otherwise
     */
    isAvailable() {
        // Check if restauraunt is closed
        if( App.isClosed ) { return false; }
        // Check the shortcut boolean is true
        if( !this.isAvailable ) { return false; }
        // Check if it is public holiday
        if( App.isHoliday ) { return this.availability.holiday; }
        // Is there infinite qty?
        if( this.isInfinite ) { return true; }
        // If QTY > 0
        if( this.qty > 0 ) { return true; }

        // Check the availability based on time and return result

        // Get values from the app object
        let lunchTime = App.lunchTime;
        let dinnerTime = App.dinnerTime;
        let closeTime = App.closeTime;
        let gmt = App.gmt; // To ensure correct time zone
        
        // Get the time in hours
        let currentDate = new Date();
        let currentHour = currentDate.getHour() + gmt;

        // Return availability based on time

        // Lunch
        if( lunchTime >= currentHour < dinnerTime ) { return this.availability.lunch; }
        // Dinner
        if( dinnerTime >= currentHour < closeTime ) { return this.availability.dinner; }
        // Out of Bounds
        return false;
    }

    /**
     * Obtains the current item price, based on factors such as time and mode
     * @returns {Number}
     */
    currentPrice() {
        // Check if it is public holiday
        if( App.isHoliday ) { return this.price.holiday; }

        // Get values from the app object
        let lunchTime = App.lunchTime;
        let dinnerTime = App.dinnerTime;
        let closeTime = App.closeTime;
        let gmt = App.gmt;

        // Get the current time in hours
        let currentDate = new Date();
        let currentHour = currentDate.getHour() + gmt;

        // Return the price based on time
        
        // Lunch
        if( lunchTime >= currentHour < dinnerTime ) { return this.price.lunch }
        // Dinner
        if( dinnerTime >= currentHour < closeTime ) { return this.price.dinner };

        // If none of the above has returned, assume tomorrow's lunch price
        return this.price.lunch;
    }

    /**
     * Obtains a list of search terms that is used to find this item
     * @returns {String[]}
     */
    searchTerms() {
        // Create an array that initially contains just the item name
        let baseArray = [this.name];
        // Concatenate the array with others
        let searchTerms = baseArray.contat( this.alternateName ).contat( this.tags ).concat( this.ingredients );
        return searchTerms;
    }
}

/**
 * An extention of the base item object with methods and fields relevant to
 * the display of a menu item in the orders / menu front-end interface
 */
class MenuItem extends Item {
    /**
     * The constructor is the same as the super "item" class
     */ 
    constructor(name, alternateName, price, description, availability,
        customisations, isOnSpecial, isAvailable, qty, isInfinite, tags,
        ingredients, image, thumbnail ) {
        super.name;
        super.alternateName;
        super.price;
        super.description;
        super.availability;
        super.customisations;
        super.isOnSpecial;
        super.isAvailable;
        super.qty;
        super.isInfinite;
        super.tags;
        super.ingredients;
        super.image;
        super.thumbnail;
    }

    // TODO - Add methods for us to display menu items
    
    /**
     * Displays the item as a menu item
     */
    showAsMenuItem() {
        // TODO Complete this STUB function
    }
    
    /**
     * Displays the full details of the item when interacted with by the user
     */ 
    showItemDetails() {
        // TODO Complete this STUB function
    }
    
    /**
     * Alternative method of searchItemDetails for use with the ordering system
     */
    showItemOrderDetails() {
        // TODO Complete this STUB function
    }
}

/**
 * An extention of the "MenuItem" class / object with relevant methods and
 * fields to support the search front-end interface
 */
class SearchItem extends MenuItem {
    /**
     * The constructor is the same as the super "item" and "MenuItem" classes
     */ 
    constructor(name, alternateName, price, description, availability,
        customisations, isOnSpecial, isAvailable, qty, isInfinite, tags,
        ingredients, image, thumbnail) {
        super.name;
        super.alternateName;
        super.price;
        super.description;
        super.availability;
        super.customisations;
        super.isOnSpecial;
        super.isAvailable;
        super.qty;
        super.isInfinite;
        super.tags;
        super.ingredients;
        super.image;
        super.thumbnail;
    }

    // TODO - Add methods for us to display search result items
    
    /**
     * Displays the item as a search result when the user is interacting
     * with the search menu
     */ 
    showSearchItem() {
        // TODO Complete this STUB function
    }
}

// Exports ------------------------------------------------------------------ /
export { Item, MenuItem, SearchItem };