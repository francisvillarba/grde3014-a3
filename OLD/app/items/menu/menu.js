/**
 * menu.js
 * 
 * This method encapsulates the functionality and the featureset of the menu
 * interface and how menu items (and details) are displayed to the user
 * and how they are controlled.
 * 
 * @author Francis Villarba <francis.villarba@me.com>
 * @version 1.0.1
 * 
 * @requires Mustache
 * @requires JavaScript ES2019 (for static class declarations)
 */

// Setup --------------------------------------------------------------------- /

import App from '../../app.js';
import { Item, MenuItem } from '../item.js';

// Default Configuration ----------------------------------------------------- /

// Categories to display
const menuCategories = [ "All Items", "Dim Sum", "Barbecue", "Rice", "Noodles", "Drinks" ];

// Modes available
const menuModes = ["Lunch", "Dinner", "Public Holiday"];


// Class Declaration --------------------------------------------------------- /

/**
 * The menu system object itself. This is to be shared and re-used across
 * multiple parts of the restaurant system from the user facing area to the
 * staff oriented interface.
 */ 
class Menu {
    // Properties
    static name = "Menu System Interface";
    static version = "1.0.0";
    static author = "Francis Villarba <francis.villarba@student.curtin.edu.au";
    
    // Object Constructor
    constructor() {
        // Declare the default configuration boundaries
        this.menuCategories = menuCategories;
        this.menuModes = menuModes;
        
        // Setup the object's instance configuration variables
        this.currentCategory = this.menuCategories[0];
        this.currentMode = this.menuModes[0];
    }
    
    // TODO Implement the menu system methods
    
}

// Exports ------------------------------------------------------------------- /
export { Menu };