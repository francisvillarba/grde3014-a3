'use strict';

/**
 * items-model
 * 
 * The items mongoose schema model for mongodb. For use with GRDE3014, Web
 * Authoring Design, Assignment 3.
 * 
 * This version has been forked from GRDE3014, Web Authoring Design, Assignment
 * 1's "./models/item.js" with further refinements and improvements to the
 * design of the schema to better suit the requirements for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@me.com>
 * @version 2.0.1
 * 
 * @requires Mongodb
 * @requires Mongoose
*/

// Setup --------------------------------------------------------------------- /
const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

// Schema -------------------------------------------------------------------- /

/**
 * Item customisation sub-document for the items model schema. Otherwise known
 * as Item options.
 * 
 * This is used to model how item customisation options are handled in the db
 * to ensure clarity, organisation and expectation for all affected areas.
*/
const ItemOptions = new Schema({
    // The name of the customisation option - to be used for the front-end
    name: {
        type: String,
        required: true,         // Customisations must have a name, obviously
        unique: false           // Non-unique as there is certainly duplicate customisation option documents in db (as each item has their own declaration)
    },
    // The default option for the customisation, defaults to none if not set
    default: {
        type: Number,       // Valid number is 0, 1, 2, 3 (None, less, regular, extra)
        required: false,        // Default to None if not specified
        enum: [0, 1, 2, 3],     // Enforce specific range using enum
        default: 0
    },
    // Is this customisation option available? Default is true
    isEnabled: {
        type: Boolean,
        required: false,
        default: true
    },
    // How much will the price change if adding or removing?
    priceChange: {
        type: Schema.types.Decimal128,
        required: false,    // Default to no price change if not provided
        default: 0.00
    }
},
{
    timestamps: true,
    versionKey: false
});

/**
 * The main schema for items in the database
*/
const ItemsSchema = new Schema({
    // The name of the item
    name: {
        type: String,
        required: true,
        unique: true,
        index: true,
    }, 
    // Alternative names for the item (if applicable)
    alternateName: [{
        type: String,
        required: false,
        index: true
        // TODO - Find a way to enable duplicate checks as unqiue: true causes issue with null
        // NOTE - Add a validator call with mongoose validator to ensure no duplicates have been added to the model as will need this for searching
    }],
    // The price at various times of day and states
    price: {
        lunch: {
            type: Schema.types.Decimal128,
            required: true
        },
        dinner: {
            type: Schema.types.Decimal128,
            required: true
        },
        holiday: {
            type: Schema.types.Decimal128,
            required: true
        },
        special: {
            type: Schema.types.Decimal128,
            required: true
        }
    },
    // The description for the item
    description: {
        type: String,
        required: true
    },
    // When is the item available
    availability: {
        lunch: {
            type: Boolean,
            required: false,
            default: true
        },
        dinner: {
            type: Boolean,
            required: false,
            default: true
        },
        holiday: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    // What Customisations can be done to the item when ordered?
    customisations: [  // Array of itemOptions sub-documents
        itemOptions
    ],
    // Is the item currently on special?
    isOnSpecial: {
        type: Boolean,
        required: false,
        default: false
    },
    // Is the item currently available to order? Irrespective of the Availability Object
    isAvailable: {
        type: Boolean,
        required: false,
        default: true
    },
    // How many of the item is available for order?
    // By default we are assuming infinite qty of the item, this value is ignored if isInfinite set to true
    qty: {
        type: Number,
        required: false,
        default: 0
    },
    // TODO - Note that we added this option for infinite qty items
    isInfinite: {
        type: Boolean,          // If true, assume no QTY restriction
        required: false,        // Front-end ignore item.qty if isInfinite = true
        default: true
    },
    // Item category tags, used for search, filters and sorting
    tags: [{
        type: String,
        required: true,
        unique: false,
        index: true
    }],
    // The primary ingredients used to create the item, used for search, filters and sorting
    ingredients: [{
        type: String,
        required: true,
        unique: false,
        index: true
    }],
    // The full resolution image for the item (For item detail view)
    image: {
        type: String,
        required: false,
        default: null
    },
    // The thumbnail version of the item, for list and grids view (for performance)
    thumbnail: {
        type: String,
        required: false,
        default: null
    }
},
{
    timestamps: true,
    versionKey: false
});

module.exports = mongoose.model( 'ItemModel', ItemsSchema );