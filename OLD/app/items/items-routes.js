'use strict';

/**
 * items-routes.js
 * 
 * The routing for the Items API
 * Adapted from GRD3014, Web Authoring Design,  Assignment 1 with improvements 
 * made to make it more RESTful and organised for Assignment 3
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.0.0
 * 
 * @requires NodeJS
 * @requires Express
 */

// Setup -------------------------------------------------------------------- /
const express = require('express');
const router = express.Router();

const controller = require('./items-controller.js');

// Routes ------------------------------------------------------------------- /

// Creation
router.post('/', controller.createItem);

// Getters
router.get('/', controller.getItems);               // All Items
router.get('/:id', controller.getItem);             // Single Item

// Modification
router.put('/:id', controller.editItem);            // Edit An Item

// Deletion
router.delete('/:id', controller.deleteItem);       // Delete an Item

// Exports ------------------------------------------------------------------ /
module.exports = router;