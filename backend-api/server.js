/**
 * server.js
 * 
 * The main server js for GRDE3014, Web Authoring Design, Assignment 3.
 * 
 * Adapted from Assignemnt 1 to better fit the updated requirements for
 * Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires NodeJS
 * @requires MongoDB
 * @requires Express
 * @requires Mongoose
 * @requires Body-Parser
 * @requires Passport
 * @requires Passport-JWT
 * @requires Connect-Mongo
 * @requires Brypt
 * @requires Mustache-Express
 * @requires dotenv
 */

// -------------------------------------------------------------------------- /
// 1. Dependencies, Settings and Application Initialisation
// -------------------------------------------------------------------------- /

// Default Configuration
require('dotenv').config;
const port = process.env.PORT || 8081; // Set 8081 if Null
const ipa = process.env.IP || 'localhost'; // The IP address 'localhost' if not c9

// Declarations
const express = require('express');                     // Express (Itself)
const mongoose = require('mongoose');                   // Mongoose for MongoDB
const bodyParser = require('body-parser');              // Req.Body
const mustacheExpress = require('mustache-express');    // Mustache Templating

const app = express();                                  // Application Init (Express App Object)

// -------------------------------------------------------------------------- /
// 2. Middleware and Plugins
// -------------------------------------------------------------------------- /

// Setup Mustache-Express
app.set( 'views', './views');                   // Where is the template stores
app.set( 'view engine', 'mustache');            // Tell Express to use Mustache as view Engine
app.engine( 'mustache', mustacheExpress() );    // Setup Mustache Express

// To enable parsing of post body requests
app.use( bodyParser.urlencoded( { extended: true } ) );         // Setup Body-Parser
app.use( bodyParser.json() );                                   // Enable JSON 

// Enable CORS (Cross-Origin Resource Sharing)
app.all( '*', function( req, res, next) {
    // Allow localhost domains only
    res.header("Access-Control-Allow-Origin", "http://localhost:8080");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-requested-with, Content-Type, Accept, Authorization");
    next();
});

// -------------------------------------------------------------------------- /
// 3. Database Connection
// -------------------------------------------------------------------------- /

mongoose.connect(
    'mongodb://245bbf4c6e183d05388e5e916e275449:nPcxWLtx9WVUgT3TQEhZ3fcB@10a.mongo.evennode.com:27017/245bbf4c6e183d05388e5e916e275449', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
)
.then(() => {
    console.log("Connected to EvenNode - A3");
})
.catch(err => {
    console.log('Problem connecting to MongoDB EvenNode Server. Exiting...', err);
    process.exit();
});

// -------------------------------------------------------------------------- /
// 4. Routing / URI Endpoints
// -------------------------------------------------------------------------- /

// Define the routes
const itemsRouter = require('./routes/items');
const usersRouter = require('./routes/users');
const ordersRouter = require('./routes/orders');
const authRouter = require('./routes/authenticate');
const indexRouter = require('./routes/index');

// Tell the express app instance to use these routes
app.use( '/api/items', itemsRouter );
app.use( '/api/users', usersRouter);
app.use( '/api/orders', ordersRouter);
app.use( '/api/auth', authRouter);
app.use( '/', indexRouter );

// -------------------------------------------------------------------------- /
// 5. Server Runtime
// -------------------------------------------------------------------------- /

app.listen( port, () => {
    console.log( `Welcome to Francis Villarba's GRDE3014 A3` );
    console.log( `Running on: ${ipa}:${port}` );
});