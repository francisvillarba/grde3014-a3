/**
 * users.js
 * 
 * The routing for the users api for the database / backend server.
 * 
 * Adapted from GRDE3014, Web Authoring Design, Assignment 1 but updated
 * to better fufil the requirements for GRDE3014, Web Authoring Design,
 * Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curitn.edu.au>
 * @version 3.0.0
 * 
 * @requires NodeJS
 * @requires Express
 */

// Setup -------------------------------------------------------------------- /
const express = require('express');
const router = express.Router();

const controller = require('../controllers/users.js');

// Core authentication system declarations
const passport = require('passport'); // Passport Core

// Routes ------------------------------------------------------------------- /

// Creation
router.post( '/', passport.authenticate( 'jwt', { session: false }), controller.createUser );      // Create user

// Listing
router.get( '/', passport.authenticate( 'jwt', { session: false }), controller.listUsers );        // List all users
router.get( '/:id', passport.authenticate( 'jwt', { session: false }), controller.listUser );      // List single user (by id)

// Modification
router.put( '/:id', passport.authenticate( 'jwt', { session: false }), controller.editUser );      // Update user information

// TODO - Delete a user account route and controller function

// Exports ------------------------------------------------------------------ /

module.exports = router;