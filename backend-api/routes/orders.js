'use strict';

/**
 * orders.js
 * 
 * The routing for the orders api.
 * Adapted from GRDE3014, Web Authoring Design, Assignment 1 with improvements
 * made to make it more RESTful and organised for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires NodeJS
 * @requires Express
 */

// Setup -------------------------------------------------------------------- /
const express = require('express');
const router = express.Router();

const controller = require('../controllers/orders.js');

// Core authentication system declarations
const passport = require('passport'); // Passport Core

// Routes ------------------------------------------------------------------- /

// Getters
router.get( '/', passport.authenticate( 'jwt', { session: false }), controller.getOrders);                             // All Items
router.get( '/:id', passport.authenticate( 'jwt', { session: false }), controller.getOrder);                            // Single Item

// Creation
router.post( '/', passport.authenticate( 'jwt', { session: false }), controller.createOrder);                          // Create an order
router.post( '/:id/item', passport.authenticate( 'jwt', { session: false }), controller.addItem);                      // Add Item to Order
router.post( '/:id/:item/note', passport.authenticate( 'jwt', { session: false }), controller.addItemNote);            // Add Note to Order Item

// Modification
router.put( '/:id', passport.authenticate( 'jwt', { session: false }), controller.editOrder);                          // Edit an order
// router.put( '/:id/:item/:note', controller.editItemNote);        // Edit an item note
// Removed as it took far too long to implement and was still incredibly buggy

// Deletion
router.delete( '/:id', passport.authenticate( 'jwt', { session: false }), controller.deleteOrder);                     // Delete an order
router.delete( '/:id/:item', passport.authenticate( 'jwt', { session: false }), controller.removeItem);                // Delete an order item
router.delete( '/:id/:item/note', passport.authenticate( 'jwt', { session: false }), controller.removeItemNote);       // Delete item note
router.delete( '/:id/:item/notes', passport.authenticate( 'jwt', { session: false }), controller.clearItemNotes);      // Clear all notes for an order item


// Exports ------------------------------------------------------------------ /
module.exports = router;