'use strict';

/**
 * index.js
 * 
 * The routing for the main pages for the database / backend server.
 * As we are not expecting users to access this page (at least non-admins),
 * we are mainly going to be routing to 404 error pages
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 * 
 * @requires NodeJS
 * @requires Express
 */

// Setup -------------------------------------------------------------------- /
const express = require('express');
const router = express.Router();

const controller = require('../controllers/index.js');

// Routes ------------------------------------------------------------------- /

// Index Page
router.get( '/', controller.getIndex );

// 404 Pages
router.get( '*/:*', controller.getError );
router.get( '*', controller.getError );

// Exports ------------------------------------------------------------------ /

module.exports = router;