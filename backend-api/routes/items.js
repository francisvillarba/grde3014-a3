'use strict';

/**
 * items.js
 * 
 * The routing for the Items API
 * Adapted from GRD3014, Web Authoring Design,  Assignment 1 with improvements 
 * made to make it more RESTful and organised for Assignment 3
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires NodeJS
 * @requires Express
 */

// Setup -------------------------------------------------------------------- /
const express = require('express');
const router = express.Router();

const controller = require('../controllers/items.js');

// Core authentication system declarations
const passport = require('passport'); // Passport Core

// Routes ------------------------------------------------------------------- /

// Getters
router.get('/', controller.getItems); // All Items
router.get('/:id', controller.getItem); // Single Item

// Filtered Getters
router.get('/menu/:menu', controller.getItemsByMenu); // Filter items based on menu (Lunch, dinner, special)
router.get('/menu/:menu/:filter', controller.getItemsMenuFiltered); // Filter items based on menu and category filters


// Creation
router.post('/', passport.authenticate( 'jwt', { session: false }), controller.createItem);

// Modification
router.put('/:id', passport.authenticate( 'jwt', { session: false }), controller.editItem); // Edit An Item

// Deletion
router.delete('/:id', passport.authenticate( 'jwt', { session: false }), controller.deleteItem); // Delete an Item

// Exports ------------------------------------------------------------------ /
module.exports = router;