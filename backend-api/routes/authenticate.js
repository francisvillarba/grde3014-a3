/**
 * authenticate.js
 * 
 * The routing for the authentication system for the backend system.
 * 
 * @author Francis Villarba <francis.villarba@student.curitn.edu.au>
 * @version 1.0.0
 * 
 * @requires NodeJS
 * @requires Express
 */

// Setup -------------------------------------------------------------------- /

// Core Imports
const express = require('express');
const router = express.Router();

// Core authentication system declarations
const passport = require('passport'); // Passport Core

// Custom Controllers
const controller = require('../controllers/authenticate.js');

// Routes ------------------------------------------------------------------- /

// Login / Authenticate (JWT Style)
router.post( '/', controller.login );


// Test route for debugging
router.post( '/test', passport.authenticate( 'jwt', { session: false }), controller.verify );

module.exports = router;