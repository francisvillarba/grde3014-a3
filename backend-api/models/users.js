'use strict';

/**
 * users.js
 * 
 * The users mongoose schema model for mongoDB. For use with GRDE3014, Web
 * Authoring Design, Assignment 3.
 * 
 * This version has been forked from GRDE3014, Web Authoring Design, Assignment
 * 1's "./models/user.js" with further refinements and improvements to the
 * design of the schema to better suit the requirements for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires mongoDB
 * @requires mongoose
 * @requires bcrypt
 * @requires express-validator
 */

// Setup -------------------------------------------------------------------- /

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Using bcrypt library for encryption of passwords
const bcrypt = require('bcrypt');                   // The encryption library
const SALT_WORK_FACTOR = 10;                        // Encryption factor
// Default of 10 to ensure low-powered server does not timeout

// Validation (input validation)
const { check, validationResult } = require('express-validator');
require('mongoose-type-email');

// Schema ------------------------------------------------------------------- /
const UsersSchema = new Schema({
    // The first name of the user
    firstName: {
        type: String,
        required: true,                 // We need to know the name of the user
        unique: false                   // Non-unique as it is common to have same name
    },
    // The last name of the user
    lastName: {
        type: String,
        required: true,                 // We need to know the name of the user
        unique: false                   // Non-unique as there is more than one family member (obviously)
    },
    // The email address of the user
    email: {
        type: mongoose.SchemaTypes.Email,       // Email Input-Validation
        lowercase: true,                        // Store it in lower case
        trim: true,                             // Trim out the white spaces
        unique: true,                           // Unique as it is the username for auth
        required: true                          // Required as it is username for auth
    },
    // Password for the user account
    password: {
        type: String,                           // String that is encrypted (Hash/salt)
        required: true                          // Obviously required for auth
    },
    // Has the user validated their email?
    isValidated: {
        type: Boolean,                          // True or false
        default: false                          // Default to not validated yet (False)
    },
    // Mobile phone number
    // TODO - Check if validator works correctly
    mobile: {
        type: String,
        required: false,
        trim: true,                             // Trim out the white spaces
        validate: {                             // Input validation
            validator: (value) => {
                return new Promise( (resolve) => {
                    resolve( check( value ).isMobilePhone( "en-AU" ) );
                    // Check that it is a mobile phone number and is australian formatted
                });
            },
            // If validation fails
            message: 'Invalid Phone Number Supplied'
        }
    },
    // Does the customer wish to recieve marketing emails?
    isMarketingEnabled: {
        type: Boolean,                          // True or false
        default: false                          // Default to false for GDPR
    },
    // What privileges is assigned to this user?
    privileges: {
        type: [Number],
        enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        // NOTE - When updating privileges from front-end use $AddToSet to prevent duplicates
        // TODO FUTURE - Add a before save / update thing in the model 
    },
    // When did the user last login?
    lastLogin: {
        type: Date,                         // Default to today's date if reated initially
        default: Date,
    },
    // When did the customer last order from us?
    lastOrder: {
        type: Date,
        default: null                       // Default to null initially
    },
    // The customer's order ID
    orderHistory: [Schema.Types.ObjectId]       // Based on _id of an order in the orders collection
    // Array of ObjectIDs
}, 
{
    timestamps: true,
    versionKey: false
});

// Middleware Functionality ------------------------------------------------- /

/**
 * Encrypt the user's password before we save the user to the database
 * 
 * Adapted from the solution made in GRDE3014, Web Authoring Design,
 * Assignment 1, but improved to use async / promises to increase performance
 * and reliability.
 */
UsersSchema.pre('save', (next) => {
    let currentUser = this;

    // If we didn't change the password, execute the callback function
    if (!currentUser.isModified('password')) {
        return next();
    }

    // Declare the encryption promise
    let encryption = new Promise((resolve, reject) => {
        // Generate a salt, for us to use with hashing
        bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
            // If there was an error, throw a promise rejection with the error
            if (err) {
                reject(err);
            }
            // Generate the hash representation of the password
            bcrypt.hash(currentUser.password, salt, (err, hash) => {
                // If there was an error, throw a promise rejection with the error
                if (err) {
                    reject(err);
                }
                // Resolve the promise with a hash
                resolve(hash);
            });
        });
    });

    // Execute the promise
    encryption.then((hash) => {
            // If resolved without rejection
            currentUser.password = hash;
            next();
        })
        .catch((err) => {
            // Catch any errors / rejections
            return next(err);
        });
});

/**
 * Encrypt the user's password before we update the user in the database
 * 
 * This is to work around the issue in GRDE3014, Web Authoring Design,
 * Assignment 1, where updating the user's password after the fact using
 * edit user URIs will result in passwords being stored in plain text.
 * 
 * Apparently, on line 1740 of Mongoose Source Code "Model.js", states
 * that findByIdAndUpdate is equivalent to findOneAndUpdate as it is
 * a wrapper, hence the use of findOneAndUpdate for our pre-hook.  
 */
UsersSchema.pre('findOneAndUpdate', (next) => {
    // Check if the password is going to be modified in this call
    // If updated, encrypt it
    // If not updated, leave it alone
    // Call next

    // Get the password field
    let password = this.getUpdate().$set.password;
    // If the field does not exist, that means we aren't updating it
    if (!password) {
        return next();
    } // Call the next part of the update chain

    // Update the password
    // Declare the encryption promise
    let encryption = new Promise((resolve, reject) => {
        // Generate a salt, for us to use with hashing
        bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
            // If there was an error, throw a promise rejection with the error
            if (err) {
                reject(err);
            }
            // Generate the hash representation of the password
            bcrypt.hash(password, salt, (err, hash) => {
                // If there was an error, throw a promise rejection with the error
                if (err) {
                    reject(err);
                }
                // Resolve the promise with a hash
                resolve(hash);
            });
        });
    });

    // Run the promise
    encryption.then((hash) => {
            // If encryption was successful, update the query
            this.getUpdate().$set.password = hash;
            next();
        })
        .catch((err) => {
            // If an error occured / promise rejection, return it to the callstack
            return next(err);
        });
});

// Functions / Instance Methods --------------------------------------------- /

/**
 * Verify the user's password against the one stored in the database by
 * comparing the hashed version of the input with the user's password field.
 * 
 * ES6 Arrow functions are not used for this method declaration as it 
 * can cause problems with binding and callbacks in Mongoose. See
 * https: //mongoosejs.com/docs/guide.html#methods for more info.
 * 
 * @param {String} input - The password for us to check
 * @return {Boolean} isMatched - True if match, false otherwise
 */
UsersSchema.methods.checkPassword = async function(input) {
    // Using bcrypt to comapre the input with the stored password
    let result = await bcrypt.compare( input, this.password );
    return result;  // Return the result of the comparison
};

// Exports ------------------------------------------------------------------ /
module.exports = mongoose.model('Users', UsersSchema);