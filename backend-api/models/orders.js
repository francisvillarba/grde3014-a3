'use strict';

/**
 * orders.js
 * 
 * The orders mongoose schema model for mongoDB. For use with GRDE3014, Web
 * Authoring Design, Assignment 3.
 * 
 * This version has been forked from GRDE3014, Web Authoring Design, Assignment
 * 1's './models/order.js' with further refinements and improvements to the
 * design of the schema to better suit the requirements for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires mongoDB
 * @requires Mongoose
 */

// Setup -------------------------------------------------------------------- /

const mongoose = require('mongoose');
const {check} = require('express-validator');
const Schema = mongoose.Schema;


// Schema ------------------------------------------------------------------- /

/**
 * Order Notes Sub-Document
 * 
 * This is used to ensure notes made to items in an order, including customisations
 * easier to store and keep organised / consistent.
 */
const OrderNote = new Schema({
    // What type of order note is it?
    flag: {
        type: Number,
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9]
        // See project specifications document for a breakdown
        // Staff, Discount, Promo, Refund, Exchange, Customisation, Request, Future, Future
    },
    // The comments / string representation of the note / customisation
    comment: {
        type: String,
        // TODO - Verify the functionality of our validator
        validate: {
            validator: (input) => {
                // Validator checks if the input is alphanumeric
                return new Promise( (resolve) => {
                    // Trim out the white space to allow our validator to function
                    let trimmedInput = input.split(' ').join('');
                    resolve( check(trimmedInput).isAlphanumeric() );    // Run validator in trimmed input
                })
                // If true, Mongoose should allow it's creation, else shows below message
            },
            // If validation fails
            message: 'Order note comments can only contain alphanumeric characters with spaces'
        }
    },
    data: {
        // Will be stored as Json, converted as string to circumvent reserved character(s) limitation
        type: String,
        get: function(data) {
            // When queried, convert String back to JSON to make this seamless
            try {
                return JSON.parse(data);
            } catch( err ) {
                // If an error occurs, return the error instead
                return err;
            }
        },
        set: function(data) {
            // When updated, convert JSON to string
            return JSON.stringify(data);
        }
    }
},
{
    timestamps: true,
    versionKey: false
});

/**
 * Order Items Sub-Document
 * 
 * This is used to represent the items that the customer has placed in
 * their order.
 */
const OrderItem = new Schema({
    // The ObjectID of the item document in the items collection
    item: {
        type: Schema.Types.ObjectId,
        required: true
    },
    // How many did the customer order?
    qty: {
        type: Number,
        required: false,
        default: 1
    },
    // The price of the item (at the time it was added to the order)
    listPrice: {    // This was added to ensure order history consistency, should item price change in the future
        type: Schema.Types.Decimal128,
        required: true
    },
    // The final price of this order item, after customisations and changes
    finalPrice: {
        type: Schema.Types.Decimal128,
        required: true
    },
    // Item notes and customisations
    notes: [OrderNote],
    // Is this particular item dine-in or take away?
    type: {
        type: Number,
        // See project specifications document for breakdown
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9]
        // Dine In, Take Away, Delivery, Scheduled DI, Scheduled TA, Scheduled DEL, Future
    }
}, 
{
    timestamps: true,
    versionKey: false
});

/**
 * Payment Information Sub-Document
 * 
 * This is used to store information about the order's payment. Used to ensure
 * consistency in how this information is stored in the database, to make it easier to
 * maintain and easier for us to display / implement for the front-end.
 */
const PaymentInfo = new Schema({
    // How did they pay for the order?
    method: {
        type: Number,
        enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],   // See project specifications document for breakdown
        // Not Paid, Cash, Card, AMEX, AliPay, Pre-Paid, Delivery Partnet, Future
        default: 0
    },
    // The total amount paid
    total: {
        type: Schema.Types.Decimal128,
        default: 0.00
    },
    // When was the order paid / processed?
    processedAt: {  // Useful for tracking and reconciliation with the bank etc.
        type: Date,
        default: null
    },
    // Who processed the payment?
    processor: {
        // This is useful for tracking / reconciliation, especially if problems occured
        type: Schema.Types.ObjectId
    }
},
{
    timestamps: true,
    versionKey: false
});

/**
 * Orders Schema
 * 
 * The primary schema for the order documents in the orders collection. It represents
 * the order that the customer has made and ties in with the Order Item and Order Note
 * Sub-Document Schemas.
 */
const OrderSchema = new Schema({
    // Who owns / created the order?
    user: {
        type: Schema.Types.ObjectId,  // The user's ObjectID in the users collection
        required: true
    },
    // What type of order is it
    type: {
        type: Number,
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9], // See project specifications document for breakdown
        // Dine In, Take Away, Delivery, Scheduled DI, Scheduled TA, Scheduled DEL, Future
        // required: true,
        // Add index to make it quicker for us to filter / search for it in the future
        default: 1,
        index: true
    },
    // Is this order a mixture of dine-in or take away?
    isMixedType: {
        // If the order is a mixture, ensure front-end checks every single order item on front-end display
        type: Boolean,
        default: false
    },
    // If scheduled order, this stores the target completion time
    schedule: { // Will be ignored if not scheduled order
        type: Date,             // If not supplied, do current + 10 mins
        default: Date.now() + 600000
    },
    // Order Status Tracking
    status: {
        type: Number,
        // For full breakdown see project specifications document
        enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        // Payment Required, Progressing, Ready, Complete, Problem, Future 
        default: 0, // Payment Required is default
        // Index to make it quicker for us to filter / search in the future
        index: true
    },
    // Have we notified the customer on ready for collection?
    isNotified: {
        // Mainly useful for places that have buzzers or sms systems in place
        type: Boolean,
        default: false
    },
    // Has this order been completed yet?
    isCompleted: { // Mainly used for the front-end to determine the interface to show
        type: Boolean,
        default: false
    },
    // The items in this order
    items: [OrderItem],
    // The order's payment information
    payment: [PaymentInfo], // Have an array to allow for dual payment (i.e. split billing, half cash / card etc.)
    // Is the order paid in full?
    isPaid: {
        type: Boolean,
        default: false
    }
}, 
{
    timestamps: true,
    versionKey: false
});

// Exports ------------------------------------------------------------------ /

module.exports = mongoose.model('Order', OrderSchema);