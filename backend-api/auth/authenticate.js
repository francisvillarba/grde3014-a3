/**
 * authenticate.js
 * 
 * Passport based authentication system middleware for the backend system
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 * 
 * @requires Passport
 * @requires Password-JWT
 * @requires dotenv
 */

// Setup -------------------------------------------------------------------- /

// Debugging
// console.log(require('dotenv').config())

// Default Configuration Declaration
require('dotenv').config();                                     // Configuration file
const SECRET_ACCESS_TOKEN = process.env.SECRET_ACCESS_TOKEN;    // Secret Key from config file .env
// Can be found in .env file at the root of the backend-api folder

// Core authentication system declarations
const passport = require('passport');                           // Passport Core
const localStrategy = require('passport-local').Strategy;       // Local DB Authentication

// The user accounts model
const UserModel = require('../models/users');                   // Users Mongoose Model

// Verification
const JWTstrategy = require('passport-jwt').Strategy;           // JWT Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt;          // Extraction of JWT from header / cookie

// Middleware --------------------------------------------------------------- /

/**
 * Defines the passport-local login strategy for the authentication system.
 * This does not utilise sessions storage in this verison (to make it easier
 * to debug in this early stage of development).
 */
passport.use( 'login', new localStrategy({
    usernameField: "email",
    passwordField: "password",
    session: false
}, async( email, password, done ) => {
    // Do a try catch
    try {
        // Debugging
        // console.log( email );
        // console.log( password );

        // Find the user that has that email address
        let user = await UserModel.findOne( {email} );

        // If we could not find the user
        if( !user ) {
            // For debugging
            console.log('Could not find the specified user based on the email');

            // Return the error message
            // let error = new Error('Credentials are incorrect');
            return done(null, false, `Username or password is invalid` );
        }

        // If the user was found, we need to verify the credentials
        let isValid = await user.checkPassword( password );
        
        // If the password is invalid
        if( !isValid ) {
            // For debugging
            console.log('The specified username / password credentials are incorrect');

            // let error = new Error('Credentials are incorrect');
            return done(null, false, `Username or password is invalid`);
        }

        // Pass the credentials (and user info) to the caller
        return done( null, user, {
            // Debug output message
            message: `Authenticated`
        });
    } catch(err) {
        // If there was a problem of any sort, pass this information up the chain
        return done(err);
    }
}));

/**
 * Defines the passport-local jwt (json web token) verification process
 * for the authentication system.
 * 
 * This handler ensures that the jwt is valid and has not been tampered
 * with or manipulated in some way.
 */
passport.use( new JWTstrategy({
    // Secret key used to sign the JWT tolen
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(), // From bearer header
    secretOrKey: SECRET_ACCESS_TOKEN // Use our .env variables
}, async( token, res ) => {
    try {
        // The token should be decoded, so let's use it to search the database
        // To check the validity of the token, the username and userid must match
        let userId = token._id;

        // For debugging
        // console.log( token );
        // console.log( userId );

        // TODO - DO more authentication checks
        // E.g. IsNotExpired, isAccessAllowed
        
        /* 
            Updated Pseudo-code for additional implementation
            // Check if expired
            // Check if id exists
                // Check privileges against token
            // Return (if above is all fine)
        */

        let userIdCheck = await UserModel.findById( userId );

        if( !userIdCheck ) {
            return res(null, false);
        }
        return res(null, userIdCheck );
        
    } catch (err) {
        // If an error occurs, launch the callback with details about the error
        return res(err);
    }
}));