'use strict';

/**
 * index.js
 * 
 * The controller for the index page of the system.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 * 
 * @requires NodeJS
 * @requires Express-Mustache
 */

// Setup -------------------------------------------------------------------- /

// Functions ---------------------------------------------------------------- /

exports.getIndex = (req, res) => {
    res.render( 'index', {
        ErrorMessage: `You have opened the backend URL. This might be a mistake. Consider going to localhost:8080 instead`,
        PageTitle: "Backend API",
        PageSubtitle: "GRDE3014 - Web Authoring Design, Assignment 3",
        PageContent: "Welcome to Francis Villarba's Web Authoring Design Assignment 3"
    });
}

exports.getError = (req, res) => {
    let data = "";

    if( req.body ) {
        data = {
            PageTitle: "404 - Not Found",
            PageSubtitle: "Sorry! It appears that we could not find what you're looking for",
            PageContent: `${JSON.stringify(req.body)}`
        }
    } else {
        data = {
            PageTitle: "404 - Not Found",
            PageSubtitle: "Sorry! It appears that we could not find what you're looking for",
            PageContent: `${req.url}`
        }
    }

    res.render( '404', data );
}