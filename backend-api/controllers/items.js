'use strict';

/**
 * items.js
 * 
 * The controller for items in the restaurant system. The controller's job is
 * to respond or run actions based on information provided by the express.router
 * for users.
 * 
 * This has been adapted from GRDE3014, Web Authoring Design, Assignment 1
 * with improvements and updates to better support Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires NodeJS
 * @requires MongoDB
 * @requires express
 * @requires Mongoose
 */

// Setup -------------------------------------------------------------------- /
const Item = require('../models/items');
const items = require('../models/items');

// Globals ------------------------------------------------------------------ /

// For our validity checks
const validFoodMenuTypes = ['all', 'lunch', 'dinner', 'special'];
const validFoodCategoryTypes = ['Dim Sum', 'BBQ', 'Noodles', 'Rice', 'Sides', 'Deserts'];
const validDrinksMenuTypes = ['Beverage', 'Tea', 'Soft Drink', 'Alcohol'];

// Getters ------------------------------------------------------------------ /

/**
 * Obtains a list of all items in the "items" collection of the database
 * @module items-controller
 * @function
 * @param {Object} req - The request, should be empty
 * @param {Object} res - The response callback / object to return to
 */
exports.getItems = (req, res) => {
    // For debugging
    // console.log(`Getting all items from database`);

    // Run the mongoDB command
    Item.find({}).sort({ 'name': 'asc' })   // Sort the responses in alphabetical order
    // If successful, run this (Yay for JS Promises!)
    .then((items) => {
        if (items.length == 0) {
            // If there is no items, send a 404 not found error JSON message
            res.status(404).send({
                msg: 'No items found'
            });
        } else {
            // Else send back json of items as a response
            res.json(items);
        }
    })
    // If this fails, do this
    .catch((err) => {
        console.log(err);
        // To make it easier to debug, list the function where error occurs
        res.status(500).send({
            msg: 'items-controller.getItems | There was a problem processing your request',
            error: err.message
        });
    });    
};

/**
 * Obtains a filtered list of all items in the "items" collection of the
 * database based on their availability.
 * @param {Object} req - The request
 * @param {String} req.params.menu - The particular menu to look for
 * @param {Object} res  - The response callback / object to return to
 */
exports.getItemsByMenu = ( req, res ) => {
    // // For debugging
    // console.log(`Getting a filtered list of items from the database based on menu type`);
    // console.log(`Requested filters: "${req.params.menu}"`);

    // Check if the menu is what we expect
    if( !req.params.menu == 'lunch' || !req.params.menu == 'dinner' || !req.params.menu == 'special' ) {
        // if it is not either of those selections, throw an error
        console.log(`Invalid menu option supplied, given ${req.params.menu}, expected lunch, dinner or special`);
        res.status(400).send({
            msg: `Invalid menu parameter suppled, expected lunch, dinner or special. Was instead given ${req.params.menu}`
        });
    } else {
        // We have the correct menu given, let's run the query on the database
        
        // Convert from ES6 to ES5 style property name to allow object literal to function
        let whereFilter = `availability.${req.params.menu}`;
        // console.log(whereFilter);
        // Not doing the above step will make it difficult for us to create the query string as
        // Simply pasting the above will throw an ES5 'Unexpected Template String' Error in Mongoose

        Item.find({}).where({ [whereFilter]: true }).sort({ 'name': 'asc' })
        .then( (items) => {
            if (items.length == 0) {
                // If there is no items, send a 404 not found error JSON message
                res.status(404).send({
                    msg: 'No items found'
                });
            } else {
                // Return the list of items
                res.json(items);
            }
        })
        .catch( (err) => {
            // An error occured while talking to the database
            console.log(err);
            // Let the front-end know by returning the error
            res.status(500).send({
                msg: 'items-controlller.getItemsByMenu | There was a problem processing your request',
                error: err.message
            });
        })
    }
};

/**
 * Obtains a filtered list of all items in the "items" collection of the
 * database based on the menu (availability) and their category filters.
 * @param {Object} req 
 * @param {Object} res 
 */
exports.getItemsMenuFiltered = (req, res) => {
    // For debugging
    // console.log(`Getting a filtered list of items from the database based on menu and catagory`);
    // console.log(`Requested menu: "${req.params.menu}"; Requested category filter: "${req.params.filter}"`);
    // console.log( !validFoodCategoryTypes.includes(req.params.filter));
    // console.log( validDrinksMenuTypes.includes(req.params.filter) );
    // console.log( !(!validFoodCategoryTypes.includes(req.params.filter) || !validDrinksMenuTypes.includes(req.params.filter)));

    // Check if the filters are filled
    if( !req.params.menu || !req.params.filter ) {
        // If we were not supplied the full arguments
        console.log(`Not enough arguments supplied, Please ensure both menu and category filter are supplied in the URI`);
        res.status(400).send({
            msg: `Not enough arguments supplied, Please ensure both menu and category filter are supplied in the URI`
        });

    } else if ( !validFoodMenuTypes.includes(req.params.menu) || !( validFoodCategoryTypes.includes(req.params.filter) || validDrinksMenuTypes.includes(req.params.filter) ) ) {
        // Check if the supplied values are valid, if they are not, throw an error
        console.log(`Invalid menu or category filter supplied, Please ensure both menu and category filters are within the valid list`);
        console.log(`Supplied menu: "${req.params.menu}" | Valid menus "${validFoodMenuTypes}"`);
        console.log(`Supplied category filter: "${req.params.filter}" | Valid category filters: "${validFoodCategoryTypes}"`);
        res.status(400).send({
            msg: `Invalid menu or category filter supplied, Please ensure both menu and category filters are within the valid list`,
            menuSupplied: `${req.params.menu}`,
            validMenus: `${validFoodMenuTypes}`,
            categoryFilterSupplied: `${req.params.filter}`,
            validCategoryFilters: `${validFoodCategoryTypes}`
        });

    } else {
        if( req.params.menu == 'all') {
            // If we have the all items set, then we just do the filter based on the category
            Item.find({}).where({"tags" : { "$all" : [req.params.filter]} }).sort({ 'name': 'asc' })
            .then( (items) => {
                if (items.length == 0) {
                    // If there is no items, send a 404 not found error JSON message
                    res.status(404).send({
                        msg: 'No items found'
                    });
                } else {
                    // Return the list of items
                    res.json(items);
                }
            })
            .catch((err) => {
                // An error occured while talking to the database
                console.log(err);
                // Let the front-end know by returning the error
                res.status(500).send({
                    msg: 'items-controlller.getItemsByMenu | There was a problem processing your request',
                    error: err.message
                });
            });
        } 
        else {
            // Filter based on both menu and category
    
            // Convert ES6 object literals to ES5 property names to allow us to dynamically create our filter string in Mongoose
            // Not doing this will cause Mongoose to rhow an ES5 'Unexpected Template String' Error
            let menuFilter = `availability.${req.params.menu}`;

            // Item.find({"tags": { "$all" : [req.params.filter]} }).where({ [menuFilter]: true }).sort({'name': 'asc' })

            Item.find({}).where({ [menuFilter]: true}).where( {"tags": { "$all" : [req.params.filter]} }).sort({ 'name': 'asc' })
            .then( (items) => {
                if (items.length == 0) {
                    // If there is no items, send a 404 not found error JSON message
                    res.status(404).send({
                        msg: 'No items found'
                    });
                } else {
                    // Return the list of items
                    res.json(items);
                }
            })
            .catch((err) => {
                // An error occured while talking to the database
                console.log(err);
                // Let the front-end know by returning the error
                res.status(500).send({
                    msg: 'items-controlller.getItemsByMenu | There was a problem processing your request',
                    error: err.message
                });
            });
        }
    }
};

/**
 * Obtains information on a single item based on a given MongoDB Object_id
 * @module itemsController
 * @function
 * @param {Object} req - The request
 * @param {Object} res - The response
 * @param {String} req.params.id - The MongoDB Object_id
 */
exports.getItem = (req, res) => {
    // For debugging
    // console.log(`Getting Item "${req.params.id}"`);

    // Run the mongoDB command
    Item.findById(req.params.id)
    // If successful run this
    .then((result) => {
        // If we found an item, send response
        res.json(result);
    })
    .catch((err) => {
        // Log the error
        console.log(err);
        // Figure out a response to the user
        if (err.kind == 'ObjectId') {
            // If the error is related to the user supplied ObjectID
            res.status(404).send({
                msg: `Item of ID "${req.params.id}" was not found`
            });
        } else {
            // Some server related error has occured
            res.status(500).send({
                msg: `itemsController.getItem | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

// Creation ----------------------------------------------------------------- /

/**
 * Creates a new item document in the MongoDB items collection
 * @module itemsController
 * @function
 * @param {Object} req - The request
 * @param {Object} req.body - User suplied json with info about the new item
 * @param {Object} res - The response
 */
exports.createItem = (req, res) => {
    // For Debugging
    // console.log('Creating new item');

    // Valid the request
    if (!req.body) {
        console.log('Was given empty body! Aborting Item Creation!!!');
        res.status(400).send({
            msg: 'The item content cannot be empty!'
        });
    } else {
        // Create the item
        // For debugging
        // console.log(req.body);
        // res.send('body received');

        let newItem = new Item(req.body);
        newItem.save()
        .then((item) => {
            res.status(201).json(item);
        })
        .catch((err) => {
            // Log the error
            console.log(err);
            // Notify the user via response
            res.status(500).send({
                msg: `itemsController.createItemPost | There was a problem processing your request`,
                error: err.message
            });
        });
    }
};

// Modification ------------------------------------------------------------- /

/**
 * Edits an item given by the item's MongoDB Object_id and json data
 *
 * Unlike Assignment 1, this method assumes that the front-end and item object
 * has determined and / or has placed $push or $pull in front of the relevant data
 * field when dealing with arrays such as ItemOptions, OrderItemNotes etc.
 *
 * @module itemController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The mongoDB Object_id of the item to edit
 * @param {Object} req.body - User supplied json with the new info to update item with
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.editItem = (req, res) => {
    // For Debugging
    // console.log(`Editing Item "${req.params.id}"`);

    if (!req.body) {
        // If the body of the post is empty
        console.log('Was given empty body! Aborting Item Edit!!!');
        res.status(400).send({
            msg: 'The item content cannot be empty!'
        });
    } else { // Edit the item
        // Set the new values
        Item.findByIdAndUpdate(req.params.id, req.body, {
            new: true
        })
        .then((result) => {
            // Print out the results (which is the updated document as new == true )
            res.json(result);
        }).catch((err) => {
            // Log the error
            console.log(err);
            if (err.kind == 'ObjectId') {
                // If it is related to the objectId not being found
                res.status(404).send({
                    msg: 'itemsController.editItemPost | Could not find the specified item'
                });
            } else {
                // If it is any other error, assume server issue
                res.status(500).send({
                    msg: 'itemsController.editItemPost | There was a problem processing your request',
                    error: err.message
                });
            }
        });
    }
};

// Deletion ----------------------------------------------------------------- /

/**
 * Deletes an item from the mongoDB items collection.
 * 
 * Do note, it is generally not a good idea to delete an item once it has been
 * created, especially if the item has been used at some point in the past.
 * This is because the deletion of the item will result in all orders that
 * reference it to reference null. 
 * 
 * This is bad because it will result in the inability for a restauruant to 
 * reconcile orders that has happened in the past as they cannot get 
 * information about what item / dish it was.
 * 
 * @module itemController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The mongoDB Object_id of the item to delete
 * @param {Object} req.body - This should be blank, will be ignored if any given
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.deleteItem = (req, res) => {
    // For debugging
    // console.log(`Deleting Item "${req.params.id}"`);

    // Run the mongoDB command promise
    Item.findByIdAndRemove(req.params.id)
    .then((result) => {
        // Notify success as well as info that it is not recommended
        res.status(200).send({
            msg: `Item _id ${req.params.id} was successfully deleted`,
            note: `NOTE! It is generally not a good idea to delete items as it could potentially cause problems with the integrity of the order history`
        });
    })
    .catch((err) => {
        // Log the error
        console.log(err);
        if (err.kind == 'ObjectId') {
            // We could not find the specified ObjectID to delete
            res.status(404).send({
                msg: `Could not find item with _id ${req.params.id}`
            });
        } else {
            // General Error Occured - Assume Server Error
            res.status(500).send({
                msg: `itemsController.deleteItem | Problem deleting a book`,
                error: err.message
            });
        }
    });
};