'use strict';

/**
 * orders.js
 * 
 * The controller that deals with orders, including order notes, item notes and
 * order payment information.
 * 
 * This verison has been forked from GRDE3014, Web Authoring Design, Assignment
 * 1's "./controllers/ordersController.js" with further refinements and
 * improvements to better suit the needs and requirements for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires NodeJS
 * @requires MongoDB
 * @requires Express
 * @requires Mongoose
 */

// Setup -------------------------------------------------------------------- /
const Orders = require('../models/orders.js');

// Listing ------------------------------------------------------------------ /

/**
 * Obtains a list of all order documents in the orders collection
 * 
 * @param {Object} req - The request
 * @param {Object} res - The response
 * @return {Undefined} - This function does not return directly
 */
exports.getOrders = (req, res) => {
    // For debugging
    // console.log(`Getting all orders from the Orders collection`);

    // Run the mongoDB command
    Orders.find({})
    .then((orders) => {
        if (orders.length == 0) {
            // There is no orders on the database yet!
            res.status(404).send({
                msg: 'No orders found',
                resolution: 'There are currently no orders in the database, perhaps make one?'
            });
        } else {
            // Respond with the list of all orders
            res.json(orders);
        }
    })
    .catch((err) => {
        // Looks like we've encountered an issue
        console.log(err); // For debugging
        res.status(500).send({
            msg: 'orders.getOrders | There was a problem processing your request',
            error: err.message
        });
    });
};

/**
 * Returns a single order, based on the given MongoDB Object_Id
 * 
 * @param {Object} req - The request
 * @param {String} req.params.id - The MongoDB Object_Id
 * @param {Object} res - The response
 */
exports.getOrder = (req, res) => {
    // For debugging
    // console.log(`Getting Order "${req.params.id}"`);

    // Run the MongoDB command
    Orders.findById(req.params.id)
    .then((result) => {
        // If we found an item, send response
        res.json(result);
    })
    .catch((err) => {
        // DRATS! An eror has occured!
        console.log(err); // For debugging
        if (err.kind == 'ObjectId') {
            // If the error is related to the user supplied ObjectId
            res.status(404).send({
                msg: `Order with ID "${req.params.id}" was not found`,
                resolution: 'Please check your Order Object_Id and try again!'
            });
        } else {
            // An uncaught error has occured, quick! Server issue!
            res.status(500).send({
                msg: `orders.gerOrder | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

// Creation ----------------------------------------------------------------- /

/**
 * Creates a new Order doucment in the MongoDB orders collection
 * 
 * @param {Object} req - The request
 * @param {Object} req.body - User supplied json with info about the new order
 * @param {Object} res - The response
 */
exports.createOrder = (req, res) => {
    // For debugging
    // console.log(`Creating new order`);

    // Validate the request
    if (!req.body) {
        console.log(`Was given empty body! Aborting Order Creation!!!`);
        res.status(400).send({
            msg: 'The order content and information cannot be empty!',
            resolution: 'Please ensure the json body is supplied and try again'
        });
    } else { // Create the Order document
        // For debugging
        // console.log(req.body);
        // res.send('body receieved');

        let newOrder = new Order(req.body); // Create an instance variable
        newOrder.save() // Attempt to save in MongoDB
        .then((order) => {
            // Return the json with details of the newly created order
            res.status(201).json(order);
        })
        .catch((err) => {
            console.log(err); // For debugging, log the error
            res.status(500).send({
                msg: 'orders.createOrder | Could not create order!',
                error: err.message
            });
        });
    }
};

/**
 * Adding an item to the order
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The object_id of the order to edit
 * @param {Object} req.body - The json containing information about the item to add to the order
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.addItem = (req, res) => {
    // For debugging
    console.log(`Adding Item "${req.params.item}" to Order "${req.params.id}"`);

    if (!req.body) {
        console.log(`Was given empty body! Aborting Item Addition!!!`);
        res.status(400).send({
            msg: 'The item body content cannot be empty!',
            resolution: 'Please ensure the json body is supplied and try again'
        });
    } else { // Add the item
        // For debugging
        // console.log(`Adding items for Order ${req.params.id}`);
        // console.log(req.body);

        // Talk to the mongoDB
        Orders.findByIdAndUpdate(req.params.id, {
            // We push to the Order's [OrderItems] field
            $push: {
                "items": req.body
            }
        }, {
            // Get output of our newly modifed Order Document
            new: true
        })
        .then((result) => {
            // Give the newly modified Order Document to the user
            res.status(201).json(result);
        })
        // If an error occurs
        .catch((err) => { 
            console.log(err); // For debugging
            if (err.kind == 'ObjectId') {
                // The order doesn't exist
                res.status(404).send({
                    msg: `ordersController.addItemToOrder | Could not find the specified order to add items to`,
                    resolution: 'Please check your Order Object_Id and try again'
                });
            } else {
                // Server related issue occured?
                res.status(500).send({
                    msg: `ordersController.addItemToOrder | There was a problem processing your request`,
                    error: err.message
                });
            }
        });
    }
};

/**
 * Adding a note to an item
 *
 * @param {Object} req - The request
 * @param {String} req.params.id - The Order_Id
 * @param {String} req.params.item - The Object_Id of the OrderItem we are noting
 * @param {Object} req.body - The notes we are adding, as a json object
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.addItemNote = (req, res) => {
    // For debugging
    // console.log(`Adding note to Item "${req.params.item}" from Order "${req.params.id}"`);

    // Check the variables
    if (!req.body) {
        console.log(`Was given empty body! Aborting Item Note Addition!!!`);
        // If the body of the request was empty, let them know
        res.status(400).send({
            msg: `The notes body cannot be empty!`,
            resolution: 'Please ensure the json body is supplied and try again'
        });
    } else {
        // For debugging
        // console.log(req.body);
        // console.log(`Editing Notes for Order _id ${req.params.id}`);
        // console.log(`Adding Notes for Item with _id ${req.params.item}`);

        // The selection query will move the $ selector to where we need it
        let selectionQuery = {
            // Set the selector $ to the Order document
            _id: `${req.params.id}`,
            "items._id": `${req.params.item}`
            // Then set selector $ to the item with that id
        };

        // Push to the selected item's notes array with data from req.body
        let pushQuery = {
            $push: {
                "items.$.notes": req.body
            }
        };

        // Actually run the commands on the database
        Orders.update(selectionQuery, pushQuery, {
            new: true
        })
        .then((result) => {
            // Prints out the results
            res.status(201).json(result);
        })
        .catch((err) => {
            // An error occured!
            console.log(err); // For debugging
            res.status(500).send({
                msg: `orders.addItemNote | Could not add note to Order_id "${req.params.id}" with target Item_id "${req.params.item}"`,
                error: err.message
            });
        });
    }
};

// Modification ------------------------------------------------------------- /

/**
 * Editing an order of Object_id with json
 *
 * !!NOTE!!
 * This function does not work with some aspects of the order schema,
 * to add items to the items array, or add notes to an order, please
 * use the functions such as addItemNote, removeItemNote etc.
 * 
 * You can call them by doing a post or delete to the following URI
 * [protocol]://[domain or ip-address]:[port]/api/orders/:orderId/:itemId/note
 *
 * @param {Object} req - The request
 * @param {String} req.params.id - The Object_id of the order to edit
 * @param {Object} req.body - The json containing information to modify
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.editOrder = (req, res) => {
    // For debugging
    // console.log(`Editing order "${req.params.id}"`);

    if (!req.body) { // If we are given an empty request body
        console.log(`Was given empty body! Aborting Order Edit!!!`);
        res.status(400).send({
            msg: 'The order modification content cannot be empty!',
            resolution: 'Please ensure the json body is supplied and try again'
        });
    } else { // Edit the item
        // Set the new values
        Orders.findByIdAndUpdate(req.params.id, req.body, {
            new: true
        })
        .then((result) => {
            // Print out results (which is updated document)
            res.status(200).json(result);
        })
        .catch((err) => {
            // An error we didn't expect occured!
            console.log(err); // For debugging, log the error
            res.status(500).send({
                msg: `orders.editOrder | Could not edit order of id "${req.params.id}"`,
                error: err.message
            });
        });
    }
};

// /**
//  * Editing or updating a particular item note in the database
//  * 
//  * @param {Object} req - The request
//  * @param {String} req.params.id - The Object_id of the order to edit
//  * @param {String} req.params.item - The Object_id of the item to edit
//  * @param {String} req.params.note - The Object_id of the note to edit / update
//  * @param {Object} req.body - The json containing the updated note information
//  * @param {Object} res - The response
//  * @return {Undefined}
//  */
// exports.editItemNote = (req, res) => {
//     // For debugging
//     console.log(`Editing Item Note "${req.params.note}" within Order "${req.params.id}" and Order Item "${req.params.item}"`);

//     // Check the request body
//     // Check if the order exists
//         // Check if the item exists
//             // Check if the note exists
//                 // Update the note
//                 // Return updated object (which include updated note)

//     if(!req.body) {
//         // If we were given an empty request body
//         console.log(`Was given an empty body! Abording Order Item Note Edit!!!`);
//         res.status(400).send({
//             msg: `The order item note modification content cannoy be empty!`,
//             resolution: 'Please ensure the json body is supplied and try again!'
//         });
//     } else {
        // Check if the order Exists

        // Attempt 1  -------------------------------------------------------- /

        // // For debugging
        // console.log( req.body );
        // console.log( `Targeting Note "${req.params.note} from Item "${req.params.item}`);

        // // Our note is a sub-document within a sub-document within the order's document (3rd level document-ception)
        // let selectionQuery = {
        //     // Move Positional $ Selector to the Order
        //     "_id": `${req.params.id}`,
        //     // Move positional $ selector to the Item
        //     "items._id": `${req.params.item}`,
        //     // Move positional $ selector to the Note
        //     "notes._id": `${req.params.note}`
        // };

        // // Our Update Query
        // let updateQuery = {
        //     "$set": {
        //         "notes.$": `${req.body}`
        //     }
        // };

        // // Run the selection and update queries on the database
        // Orders.updateOne( selectionQuery, updateQuery, { new: true } )
        // .then( (result) => {
        //     // Export the results to the user
            
        //     // For debugging
        //     console.log( 'Update successful' );

        //     res.status(200).json(result);
        // })
        // .catch( (err) => {
        //     // An error has occured
        //     console.log( `orders.editItemNote | An error has occured` );
        //     if( err.kind == 'ObjectId' ) {
        //         // Could not find either the order or item requested
        //         console.log( `Could not find order "${req.param.id}" or item "${req.param.item}"`);
        //         res.status(404).send({
        //             msg: `Could not find the Order, Item or Note specified`,
        //             resolution: `Please check the Order_Id for Order, Item and Note and try again!`,
        //             error: err.message
        //         });
        //     } else {
        //         // A more generalised error has occured, time to throw a big one
        //         console.log( `A generalised / non-accounted error has occured` );
        //         res.status(500).send({
        //             msg: `orders.editItemNote | A problem occured when attempting to edit or update an order item note`,
        //             error: err.message
        //         });
        //     }
        // });

        // Attempt 2 -------------------------------------------------------- /

        // // For debugging
        // console.log(`Targeting Note "${req.params.note} from Item "${req.params.item}`);
        // console.log( req.body );

        // // Our note is a sub-document within a sub-document within the order's document (3rd level document-ception)

        // Orders.findById( req.params.id )    // Find the Item First
        // .then( (order) => {
        //     // For debugging
        //     console.log(`Order Document Output`);
        //     console.log(order);
        //     console.log(`Checking if the item exists`);
        //     console.log(`Item Object_Id to search for: ${req.params.item}`);

        //     // Check if item exists
        //     let itemExists = false;
        //     order.items.forEach( (element) => {
        //         if( !itemExists ) {
        //             // For debugging
        //             // console.log( element._id );
        //             if( element._id = `${req.params.item}` ) {
        //                 itemExists = true;
        //             }
        //         }
        //     });

        //     // For debugging
        //     console.log(`Item Exists: ${itemExists}`);
            
        //     if( !itemExists ) {
        //         // If the specified item does not exist
        //         res.status(404).json({
        //             msg: `Could not find the item "${req.params.item} in order "${req.params.id}"`,
        //             resolution: `Check your item object_id and try again!`
        //         });
        //     } else {
        //         // For debugging
        //         console.log(`Checking if the note exists`);
                
        //         let noteExists = false;
        //         order.items.id(req.params.item).notes.forEach( (note) => {
        //             if( !noteExists ) {
        //                 // For debugging
        //                 console.log( note._id );

        //                 if( note._id = `${req.params.note}` ) {
        //                     noteExists = true;
        //                 }
        //             }
        //         });

        //         // For debugging
        //         console.log(`Note Exists: ${noteExists}`);

        //         if( !noteExists ) {
        //             // If the specified note does not exist
        //             res.status(404).json({
        //                 msg: `Could not find the note "${req.param.note} in item ${req.params.item} from order "${req.params.id}`,
        //                 resolution: `Check your note object_id and try again!`
        //             });
        //         } else {
        //             // Let's go and update the note
        //             order.items.id(req.params.item).notes.id(req.params.item) = req.body;
        //             order.save();
        //         }
        //     }
        // })
        // .catch( (err) => {
        //     // An error occured
        //     console.log( `orders.editItemNote | A general / non-accounted error has occured` );
        //     res.status(500).json({
        //         msg: `orders.editItemNote | There was a problem processing your request`,
        //         error: err
        //     });
        // });

        // For Debugging
        // console.log(`Targeting Note "${req.params.note} in Item "${req.params.item}" from Order "${req.params.id}`);
        // console.log( req.body );

    //     Orders.findById( req.params.id )
    //     .then( (order, err) => {
    //         // If an error occured while trying to find the order
    //         if( err ) {
    //             // Output notice to console
    //             console.log(`An error occured while querying the database`);
    //             if( err.kind == 'ObjectId' ) {
    //                 // The error is based on objectId
    //                 console.log(`Could not find Order "${req.params.id}`);
    //                 res.status(404).json({
    //                     msg: `Could not find Order "${req.params.id}"`,
    //                     resolution: `Please check the order object_Id and try again!`,
    //                     error: err
    //                 });
    //             } else {
    //                 // A generalised error has occured
    //                 console.log(err);   // Print out full error object
    //                 res.status(500).json({
    //                     msg: `orders.editItemNote | There was a problem processing your request`,
    //                     error: err
    //                 });
    //             }
    //         }
    //         console.log(order.items);

    //         order.populate( { path: 'items.notes' } );
            
    //         let targetNote = order.items.id(`${req.params.item}`).notes.id(`${req.params.note}`);
    //         // console.log(targetNote);
    //         targetNote.set( req.body );
    //         return order.save();
    //     })
    //     .then( (res) => {
    //         // It was successful

    //         // For debugging
    //         console.log(`Success!`);
    //         console.log(res);

    //         // Send response to the user
    //         res.status(200).send( res );
    //     })
    //     .catch( (err) => {
    //         // An error occured
    //         console.log( `order.editItemNote | An error occured while performing the action`);
    //         console.log(err);
    //         res.status(500).json({
    //             msg: `order.editItemNote | There was a problem processing your request`,
    //             error: err
    //         });
    //     });
    // }

//         // Final Attempt -------------------------------------------------------- /
//         let selectionQuery = {
//             _id: `${req.params.id}`,              // Positional $ at order
//             "items._id": `${req.params.item}`,      // Positional $ at Item
//         }

//         Orders.findById( req.params.id )
//         .then( (order) => {
//             // For debugging
//             console.log( order.items.id(req.params.item).notes.id(req.params.note));

//             let notesDocument = order.items.id(req.params.item).notes.id(req.params.note);
//             notesDocument.set( req.body );
//             console.log( notesDocument );
//             order.save();
//         })
//         .catch( (err) => {
//             // An error occured
//             console.log( `order.editItemNote | An error occured while performing the action`);
//             console.log(err);
//             res.status(500).json({
//                 msg: `order.editItemNote | There was a problem processing your request`,
//                 error: err
//             });
//         });
//     }
// };

// Deletion ----------------------------------------------------------------- /

/**
 * Deletes an order
 *
 * @param {Object} req - The request
 * @param {String} req.params.id - The object_id of the order to modify
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.deleteOrder = (req, res) => {
    // For debugging
    console.log(`Deleting order "${req.params.id}"`);

    // Check if the order exists first
    Orders.findById( req.params.id )
    .then( (result) => {

        // Check some variables
        let isPaid = result.isPaid;
        let isCompleted = result.isCompleted;
        let status = result.status == 0;

        // For debugging
        // console.log(result);
        // console.log(isPaid);
        // console.log(isCompleted);
        // console.log(status);

        if(!isPaid && !isCompleted && status) {
            // If not paid, not completed and status is zero (PaymentRequired)

            // Safe to delete
            Orders.findByIdAndRemove( req.params.id )
            .then( (result) => {
                res.status(200).send({
                    msg: `Order "${req.params.id}" was deleted`
                });
            })
            .catch( (err) => {
                console.log(err);                               // For debugging
                // An error has occured
                if( err.kind == 'ObjectId' ){
                    // The order doesn't exist
                    res.status(404).send({
                        msg: `orders.deleteOrder | Order "${req.params.id}" was not found!`,
                        resolution: 'Please check your Order Object_Id and try again!'
                    });
                } else {
                    res.status(500).send({
                        msg: `orders.deleteOrder | There was a problem processing your request`,
                        error: err.message
                    });
                }
            });
        } else {
            /*
             * It is not safe to delete as deletion can have an effect on order
             * history integrity, causing problems for accounting. Deleting the
             * order will cause EFTPOS Merchant totals and POS Cash would be
             * incorrect and WILL cause headaches when doing accounting.
            */
            res.status(400).send({
                msg: `orders.deleteOrder | Could not delete Order "${req.params.id}"`,
                resolution: "Order has already been paid and / or processed. \n Deleting this order will result in errors in order history and sales reconciliation!"
            });
        }
    })
    .catch( (err) => {
        // An error has occured
        console.log(err);                                       // For debugging
        if( err.kind == 'ObjectId' ){
            // The order doesn't exist
            res.status(404).send({
                msg: `orders.deleteOrder | Order "${req.params.id}" was not found!`,
                resolution: 'Please check your Order Object_Id and try again!'
            });
        } else {
            res.status(500).send({
                msg: `orders.deleteOrder | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

/**
 * Removes all items from the order based on a given itemId
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The object_id of the order to modify
 * @param {String} req.params.item - The object_id of the item to remove
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.removeItem = (req, res) => {
    // For debugging
    console.log(`Removing Item "${req.params.item}" from Order "${req.params.id}"`);

    // Strip the notes from the order
    // Use lean as we don't actually need the output to speed up MongoDB processing speed
    Orders.findById(req.params.id).lean() // Find if it exists first
    .then((result) => {
        // For debugging
        // console.log(result);
        // console.log(result.items.length);

        // Check if the Item exists
        let itemExists = Orders.find({}).where({
            "items._id": `${req.params.item}`
        }).lean().count()
        .then((result) => {
            return result;
        });
        // No need to catch error on OrderId as we have already checked this earlier

        if (itemExists) {
            // Clear the notes attached to the item
            Orders.update({
                // Set our selector to our item
                _id: `${req.params.id}`,
                "items._id": `${req.params.item}`
            }, {
                // Unsure if this is necessary in NoSQL databases
                // By using set, we are making the item's [OrderNotes] empty to ensure no dangling records
                $set: {
                    "items.$.notes": []
                }
            })
            .catch((err) => {
                // An error occured while trying to access the item's notes, deal with it here
                console.log(err);
                res.status(500).send({
                    msg: `orders.removeItem | There was a problem processing your request`,
                    error: err.message
                });
            });

            // Pull the item from the Order Items Sub-Document Array
            Orders.update({
                _id: `${req.params.id}`
            }, {
                // Pull the item from order's [OrderItems]
                $pull: {
                    "items": {
                        _id: `${req.params.item}`
                    }
                }
            }, {
                new: true
            })
            .then((result) => {
                // Return a copy of the newly modified Order
                res.status(200).json(result);
            })
            .catch((err) => {
                // An error occured while trying to pull the item from the order, deal with it here
                console.log(err);
                res.status(500).send({
                    msg: `orders.removeItem | There was a problem processing your request`,
                    error: err.message
                });
            });
        } else {
            // Item doesn't exist, throw an error (!itemExists)
            console.log(`Item "${req.params.item}" does not exist in Order "${req.params.id}"`);
            res.status(404).send({
                msg: `orders.removeItem | Could not find OrderItem "${req.params.item}" in Order ${req.params.id}`,
                resolution: 'Please check your Item Object_Id and try again!'
            });
        }
    })
    .catch((err) => {
        console.log(err); // For debugging
        if (err.kind == 'ObjectId') {
            // If the error is related to the Order's ObjectId
            res.status(404).send({
                msg: `orders.removeItem | Could not find Order "${req.params.id}"`,
                resolution: 'Please check your Order Object_Id and try again!'
            });
        } else {
            // A general error occured that we haven't accounted for
            res.status(400).send({
                msg: `orders.removeItem | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

/**
 * Removes a note from an item
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The Order's Object_Id
 * @param {String} req.params.item - The Object_id of the OrderItem we are editing
 * @param {String} req.params.note - The Object_id of the OrderNote we are removing
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.removeItemNote = (req, res) => {
    // For debugging
    // console.log(`Removing Note "${req.params.note}" from Item "${req.params.item}" from Order "${req.params.id}"`);

    // For debugging
    // console.log(req.body);
    // console.log(`Pulling Notes for Order ${req.params.id}`);
    // console.log(`Targeting Notes for Item with Index ${req.params.index}`);

    // The selection query will set the selector $ to where we need it
    let selectionQuery = {
        // Set the selector $ to the order document
        _id: `${req.params.id}`,
        // Set the selector $ to the item with the corresponding ID
        "items._id": `${req.params.item}`
    };

    // The query where we pull the item of a given note
    let pullQuery = {
        $pull: {
            "items.$.notes": {
                _id: `${req.params.note}`
            }
        }
    };

    // Now we run the commands on the database
    Orders.update(selectionQuery, pullQuery, {
        new: true
    })
    .then((result) => {
        // Prints out the results
        res.status(200).json(result);
    })
    .catch((err) => {
        // An error occured!
        console.log(err);
        if (err.kind == 'ObjectId') {
            // Cannot find either the Order or Item requested
            res.status(404).send({
                msg: `Order or Item not found`,
                resolution: 'Please check your Order Object_Id or Item Object_Id and try again!',
                error: err.message
            });
        } else {
            // General Error, Throw server error
            res.status(500).send({
                msg: `orders.removeItemNote | A problem occured when attempting to remove a note from Order_id "${req.params.id}"" with the target Item_id "${req.params.item}"`,
                error: err.message
            });
        }
    });
};

/**
 * Clears all notes associated with a given order's item
 *
 * @module ordersController
 * @function
 * @param {Object} req - The request
 * @param {String} req.params.id - The Order's Object_Id
 * @param {String} req.params.item - The Object_id of the OrderItem we are editing
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.clearItemNotes = (req, res) => {
    // For debugging
    // console.log(`Clearing notes for Item "${req.params.item}" from Order "${req.params.id}"`);

    // Strip the notes from the order
    // Use lean as we don't actually need the output to speed up MongoDB processing speed
    Orders.findById(req.params.id).lean() // Find if it exists first
    .then((result) => {
        // For debugging
        // console.log(result);
        // console.log(result.items.length);

        // Check if the Item exists
        let itemExists = Orders.find({}).where({
                "items._id": `${req.params.item}`
            }).lean().count()
            .then((result) => {
                return result;
            });
        // No need to catch error on OrderId as we have already checked this earlier

        if (itemExists) {
            // Clear the notes attached to the item
            Orders.update({
                // Set our selector to our item
                _id: `${req.params.id}`,
                "items._id": `${req.params.item}`
            }, {
                // Unsure if this is necessary in NoSQL databases
                // By using set, we are making the item's [OrderNotes] empty to ensure no dangling records
                $set: {
                    "items.$.notes": []
                }
            })
            .then((result) => {
                // After we update, send a response back to the user
                res.status(200).send({
                    msg: `Successfully cleared notes for Item "${req.params.item}" from order "${req.params.id}"`
                });
            })
            .catch((err) => {
                // An error occured while trying to access the item's notes, deal with it here
                console.log(err);
                res.status(500).send({
                    msg: `orders.removeItem | There was a problem processing your request`,
                    error: err.message
                });
            });
        } else {
            // Specified item does not exist in this order
            console.log(`Item ""${req.params.item}" does not exist in Order "${req.params.id}"`);
            res.status(404).send({
                msg: `orders.clearItemNotes | Could not find Item "${req.params.item}" in Order "${req.params.id}"`,
                resolution: 'Please check your Item Object_Id and try again!'
            });
        }
    })
    .catch((err) => {
        console.log(err); // For debugging
        // An error occured
        if (err.kind == 'ObjectId') {
            // If it is related to the Order's ObjectId not being found
            res.status(404).send({
                msg: `orders.clearItemNotes | Could not find Order "${req.params.id}"`,
                resolution: 'Please check your Order Object_Id and try again!'
            });
        } else {
            // A more generalised error has occured, assume server issue
            res.status(500).send({
                msg: `orders.clearItemNotes | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};