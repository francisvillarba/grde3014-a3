'use strict';

/**
 * users.js
 * 
 * The controller that deals with users and user accounts. Responds with
 * relevant data, based on the route / data provided and / or traversed
 * by the user.
 * 
 * THis version has been forked from GRDE3014, Web Authoring Design, Assignment
 * 1's "./controllers/usersController.js" with furhte rrefinements and
 * improvements to better suit the requirements for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 * 
 * @requires nodeJS
 * @requires mongoDB
 * @requires express
 * @requires mongoose
 */

// Setup -------------------------------------------------------------------- /
const Users = require('../models/users.js');

// Listing ------------------------------------------------------------------ /

/**
 * Display a list of all users in the database
 * 
 * @param {Object} req - The request
 * @param {Object} res - The response
 * @return {Undefined} - NO returns from this method
 */
exports.listUsers = (req, res) => {
    // For debugging
    // console.log(`Listing all users in the users collection`);

    // Run Mongoose Commands
    Users.find({})
    .then((result) => {
        if (result.length == 0) {
            // We didn't find any users in the database
            res.status(404).send({
                msg: `No users found!`
            });
        } else {
            // We found users, let's display them
            res.status(200).json(result);
        }
    })
    .catch((err) => {
        // An error has occured, likely communication or server related
        console.log(err);
        res.status(500).send({
            msg: `users.listUsers | There was a problem processing your request`,
            error: err.message
        });
    });
};

/**
 * Display a single user from the database
 *
 * @param {Object} req - The request
 * @param {String} req.params.id - The MongoDB ObjectId for the user we are finding
 * @param {Object} res - The response
 * @return {Undefined} - No data is returned if method is called directly
 */
exports.listUser = (req, res) => {
    // For debugging
    // console.log(`Listing User "${req.params.id}"`);

    // Run Mongoose commands
    Users.findById(req.params.id)
    .then((result) => {
        // We found the book, return a json response with the book's information
        res.status(200).json(result);
    })
    .catch((err) => {
        // An error has occured
        console.log(err);
        if (err.kind == 'ObjectId') {
            // If the error is related to the ObjectID
            res.status(404).send({
                msg: `The user with ID "${req.params.id}" could not be found`
            });
        } else {
            // A more generalised error, assume server error
            res.status(500).send({
                msg: `users.listUser | There was a problem processing your request`,
                error: err.message
            });
        }
    });
};

// Creation ----------------------------------------------------------------- /

/**
 * Creates a user in the database
 * 
 * @param {Object} req - The request
 * @param {Object} req.body - The body of the request in application/json format
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.createUser = (req, res) => {
    // For debugging
    // console.log(`Creating new user`);

    if (!req.body) {
        // We cannot have an empty body tag
        console.log("Was given empty body! Aborting User Creation!!!");
        res.status(400).send({
            msg: `The user content body cannot be empty!`
        });
    } else {
        // Convert from json to js object
        let newUser = new Users(req.body);

        // Time to save!
        newUser.save()
        .then((result) => {
            // If all was successful, return a copy of our new user account
            res.status(201).json(result);
        })
        .catch((err) => {
            // Oh dear! Looks like an error has occured
            console.log(err);
            res.status(500).send({
                msg: `Could not create user`,
                error: err.message
            });
        });
    }
};

// Modification ------------------------------------------------------------- /

/**
 * Edit / Update the user's information
 * 
 * @param {Object} req - The request
 * @param {String} req.params.id - The id of the user to modify
 * @param {Object} req.body - The body that contains the edits in json format
 * @param {Object} res - The response
 * @return {Undefined}
 */
exports.editUser = (req, res) => {
    // For debugging
    // console.log(`Modifying user "${req.params.id}"`);

    if (!req.body) {
        // If the body of the request is empty, we need to say something!
        console.log('Was given empty body! Aborting User Edit!!!');
        res.status(400).send({
            msg: `The user body content cannot be empty!`
        });
    } else {
        Users.findByIdAndUpdate(req.params.id, req.body, {
            new: true
        })
        .then((result) => {
            // If all is well, let's return a copy of the newly modified entry
            res.status(200).json(result);
        })
        .catch((err) => {
            // Log the error
            console.log(err);

            if (err.kind == 'ObjectId') {
                // We can't find the user
                res.status(404).send({
                    msg: `users.editUser | Could not find user with id "${req.params.id}"`
                });
            } else {
                // A more generalised / uncaught error has occured
                res.status(500).send({
                    msg: `users.editUser | There was a problem processing your request`,
                    error: err.message
                });
            }
        });
    }
};

// Deletion ----------------------------------------------------------------- /

// TODO - Delete a user account