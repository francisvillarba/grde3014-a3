/**
 * authenticate.js
 * 
 * The authentication controller for the backend-api in GRDE3014, Web Authoring
 * Design Assignment 3
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 * 
 * @requires nodeJS
 * @requires express
 * @requires jsonwebtoken
 * @requires passport
 */

// Setup -------------------------------------------------------------------- /

// Default Configuration Declaration
require('dotenv').config();                                     // Configuration File
const SECRET_ACCESS_TOKEN = process.env.SECRET_ACCESS_TOKEN;    // Secret Key from Config
const TOKEN_EXPIRY = process.env.TOKEN_EXPIRY;                  // Token Validity

const passport = require('passport');
const jwt = require('jsonwebtoken');

const auth = require('../auth/authenticate.js');
const Users = require('../models/users.js');

// Functions ---------------------------------------------------------------- /

/**
 * Authenticates the user based on the given username and password combination
 * @param {Object} req - The request
 * @param {Object} res - The response
 * @param {Object} callback - The callback for the system to run after login
 */
exports.login = async (req, res, callback) => {
    try {
        passport.authenticate( 'login', async ( err, user, message ) => {
            try {
                if( err ) {
                    /*  If ther was a problem trying to use the passport.use('login')... from
                        authenticate.js in /auth/, output some detailed debug messages and throw
                        the error up the callstack / chain
                    */
                    console.log( `An error occured while trying to authenticate` );
                    console.log( err );
                    return res.status(500).json({
                        msg: message,
                        error: err
                    });
                }
                if ( !user ) {
                    // If no user was found, based on the supplied information
                    console.log( `No user account was found with given email` );
                    return res.status(400).json({
                        msg: message,
                        resolution: 'Please check your credentials and try again',
                        error: err
                    });
                }
                // Authentication was successful, let's setup their token
                let payload = {
                    // The payload is what we are going to store in their token
                    _id: user._id,                  // Their userID
                    email: user.email,              // Their Email Address
                    privileges: user.privileges,    // Their Access Privileges
                    when: Date.now()                // When did we make this token?
                }

                // Create the token
                let signedToken = jwt.sign( payload, SECRET_ACCESS_TOKEN, { expiresIn: TOKEN_EXPIRY });
                // Return JSON to caller
                return res.status(200).json( {
                    token: "Bearer " + signedToken,
                    user: user._id,
                    expires: TOKEN_EXPIRY,
                    response: message
                });
            }
            catch ( err ) {
                // Output detailed debugging messages
                console.log( `A general error occured` );
                console.log(err);
                return res.status(500).json({
                    msg: message,
                    error: err
                });
            }
        })(req, res, callback);
    } catch ( err ) {
        // Output detailed debugging messages
        console.log( `An error occured while trying to authenticate` );
        console.log( err );
        res.status(500).json({
            msg: `An error occured while trying to authenticate`,
            error: err
        });
    }
};

/**
 * Checks the validity of the token
 * @param {Object} req - The request
 * @param {Object} res - The response
 */
exports.verify = (req, res ) => {
    res.status(200).json({
        msg: "Successfully accessed protected route~!"
    });
};