/**
 * webpack.config.js
 * 
 * Custom webpack configuration file for the restaurant application.
 * Inspired by the tutorial :Webpack 4 Tutorial - Getting Started for Beginners"
 * from https://www.youtube.com/watch?v=TzdEpgONurw but customised greatly
 * to better suit the needs and requirements for GRDE3014, Web Authoring Design,
 * Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 */

// Imports ------------------------------------------------------------------ /

const HtmlWebPackPlugin = require("html-webpack-plugin");           // HTML
const MiniCssExtractPlugin = require("mini-css-extract-plugin");    // CSS
const DotEnv = require('dotenv-webpack');                           // Environment Variables
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');   // Favicons Generator Plugin

// Configuration ------------------------------------------------------------ /

module.exports = {
    entry: "./src/index.js",                // Define the app entry point (as it is different from our package.json)
    mode: 'development',                     // Default to none if we run webpack directly, dev when we are coding, production for final build export
    optimization: {
        minimize: true                      // Minify our output
    },
    node: {
        global: false,
        __filename: false,
        __dirname: false,
        fs: "empty"
    },
    module: {
        // Rules on files and how web-pack should handle these
        rules: [
            {
                // Find all HTML files to package using regex (regular expressions)
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: {
                            // Minimise the output html file
                            minimize: true
                        }
                    }
                ]
            },
            {
                // Find all JS files that aren't from npm
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        sourceMaps: true
                    }
                }
            }, 
            {
                // Find all images to package
                test: /\.(gif|jpg|png|svg)$/,
                use: [
                    "file-loader",
                    {
                        // Optimise the images in output
                        loader: "image-webpack-loader",
                        options: {
                            // Disable in dev mode
                            bypassOnDebug: true,
                            // Global optimisation disable switch
                            disable: false, // We want it to optimise if we aren't on dev
                            mozjpeg: {
                                // Optimise JPEG files
                                progressive: true,
                                quality: 65
                            },
                            optipng: {
                                // Optimise PNG files
                                enabled: true
                            },
                            pngquant: {
                                // Optimise PNG files further
                                quality: [0.7, 0.9], // Quality Level between 70-90%
                                speed: 4
                            },
                            gifsicle: {
                                // Optimise GIFs
                                interlaced: false
                            }
                        }
                    }
                ]
            },
            {
                // Find all sass files
                test: /\.scss$/,
                use: [
                    "style-loader",     // Inject the style into the DOM
                    "css-loader",       // Translate CSS into JS
                    "sass-loader"       // Turn SCSS into CSS
                ]
            },
            {
                // Find all mustache template files
                test: /\.mustache$/,
                loader: 'mustache-loader',
                options: {
                    // Additional Configuration Options
                    tiny: false     
                }
            }
        ]
    },
    // External plugins to help with webpack rendering
    plugins: [
        new HtmlWebPackPlugin({
            // Source File
            template: "./src/index.html",
            // Destination
            filename: "./index.html"
        }),
        new MiniCssExtractPlugin({
            // Source File
            filename: "[name].css",
            // Extracted CSS Destination Filenames
            chunkFilename: "[id].css"
        }),
        // Environment Variables
        new DotEnv(),
        // Auto generate favicons so chrome would STOP COMPLAINING!
        new FaviconsWebpackPlugin('./src/img/logo.png')
    ]
};