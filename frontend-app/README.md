# GlobalOne Restaurant System SPA #
## Web Authoring Design, Assignment 3 ##

Created by Francis Villarba < <francis.villarba@student.curtin.edu.au> >

---

**NOTICES**

This project utilises webpack and thus, the method for running this web application will differ from the norm.

The purpose of webpack, in this project, is to have an easy way for us to bundle, optimise and compact our project so that it can run as quickly as possible on a production environment.

Strategies in place with our particular webpack configuration include:

* Drastic reduction of HTML get requests by a client browser
* Compression / Optimisation of multimedia conent
* Compilation of SCSS in a single step
* Asset Management
* Easier distribution of production / live code

---

_Running Development Mode_

> _**"Nodemon is not supported in this configuration"**_

To run in development mode, run the following command you are within the root directory of the frontend-app folder

```
npm run dev
```

This will launch webpack-dev-server, which will work similar to nodemon.

NOTE! webpack-dev-server can take a while to launch, please wait for the message [wdm]: compiled successfully before attempting to view the site at 'localhost:8080'.

---

_Running the project_

> _**"Run production version of the project"**_

If you want to build and run the production version of the application, run the following commands when you are within the root directory of the frontend-app folder.

```
npm run build
npm run app
```

Webpack will compile an optimised version of the application into the ./dist/ directory when "npm run build" is executed, followed by running live-server on ./dist/ when "npm run app" is executed.

From there, you simply navigate to 'localhost:8080' in your browser of choice and the application should be ready for use!

---

## /src Directory ##

> The source folder organisation explained

| Directory Name | Purpose |
| -------------- | ------- |
| components     | Core App Components and Objects |
| controllers    | Page controllers |
| img | Images to be packaged & compressed by webpack |
| partials | Partial templates, used to help with displaying of certain items, such as menu item |
| scss | Sass source files |
| views | Templates for use with page controllers |

---

