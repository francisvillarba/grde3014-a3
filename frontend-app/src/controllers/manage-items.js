/**
 * manage-items.js
 * 
 * The item management page and controller for the SPA
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.5.3
 */

// Imports ------------------------------------------------------------------ /
// SPA Components
import { App } from '../components/app.js';                     // The core SPA system component
import { Modal } from '../components/modal.js';                 // The modal system component

// Import assets for webpack
const template = require('../views/manage-items.mustache'); // The menu page mustache template
const searchToolTemplate = require('../partials/manage-items-search.mustache'); // The search tool
const menuGridTemplate = require('../partials/menu_grid.mustache'); // The menu items grid (for all items, sold out and discontinued)
const creationToolTemplate = require('../partials/manage-items-create.mustache');   // The item creation tool

// Functions ---------------------------------------------------------------- /

/**
 * Helper function that displays all discontinued items in the database
 */
function displayDiscontinuedPartial() {
    // TODO FUTURE - Implement this feature
    // Let the user know that this feature is not yet implemented
    Modal.displayWarning(
        'Not yet implemented',
        'The display discontinued items tool is not yet implemented. Please check back later.',
        'Okay'
    );
    Modal.setPrimaryAsClose();
    // Implementation would require the database items collection to be updated with additional fields...
};

/**
 * Helper function that displays all sold out items in the database
 */
function displaySoldOutPartial() {
    // TODO FUTURE - Implement this feature
    // Let the user know that this feature is not yet implemented
    Modal.displayWarning(
        'Not yet implemented',
        'The display sold out items tool is not yet implemented. Please check back later.',
        'Okay'
    );
    Modal.setPrimaryAsClose();
};

/**
 * Helper function that displays all items and prepares it for use
 */
function displayAllItemsPartial() {
    // Declare the root element for our partial
    let partialRoot = document.querySelector('#partial-target');
    // Clear the target root of anything within
    partialRoot.innerHTML = '';
    // Generate our template output
    let templateOutput = menuGridTemplate({
        ToolName: 'All Items (A-Z)'
    });
    // Append the tool to the page
    partialRoot.insertAdjacentHTML('beforeend', templateOutput);
    // Update our navigation to display the current tool
    updateToolNavigation('#all-items-tool');

    // TODO - Prepare it's use
};

/**
 * Helper function that displays the item creation tool and prepares it for use
 */
function displayCreationPartial() {
    // Declare the root element for our partial
    let partialRoot = document.querySelector('#partial-target');
    // Clear the target root of anything within
    partialRoot.innerHTML = '';
    // Generate our template output
    let templateOutput = creationToolTemplate();
    // Append the creation tool to the page
    partialRoot.insertAdjacentHTML('beforeend', templateOutput);
    // Update our navigation to display the current tool
    updateToolNavigation('#create-tool');

    // TODO - Prepare it's use
};

/**
 * Helper function that displays the search tool and prepares it for use
 */
function displaySearchPartial() {
    // Declare the root element for our partial
    let partialRoot = document.querySelector('#partial-target');
    // Clear the target root of anything within
    partialRoot.innerHTML = '';
    // Generate our template output
    let templateOutput = searchToolTemplate({
        ToolName: `Item Search Tool`
    });
    // Append the search partial to the page
    partialRoot.insertAdjacentHTML('beforeend', templateOutput);
    // Update our navigation to display this link
    updateToolNavigation('#search-tool');

    // TODO - Prepare it's use
};

/**
 * Helper function that displays the home page
 */
function displayHomePartial() {
    // add if statement just in case the system doesn't show this page (authentication error etc.).
    if( document.querySelector('#partial-target') != null ) {
        // Declare the root element for our partial
        let partialRoot = document.querySelector('#partial-target');

        // Clear the partial
        partialRoot.innerHTML = '';

        // Add our home page content (ideally i should have had a template for this...)
        partialRoot.insertAdjacentHTML('beforeend',
            '<article class="message is-link"><div class="message-header"><p>Welcome</p></div><div class="message-body"><p>Welcome to the Item Management System. This system allows you to edit menu items.</p><br><p> To get started, select a tool from the menu toward the left hand side of the screen.</p></div></article>'
        );

        // Update our navigation to display this link
        updateToolNavigation('#management-home');
    }
};

/**
 * Helper function that ensures the correct link is selected
 * @param {String} nextTool - What is the next tool for us to highlight?
 */
function updateToolNavigation( nextTool ) {
    // Populate our selectors
    let toolsMenu = document.querySelectorAll('#tools-selection > li');
    
    // For each element in our toolsMenu list, we clear their class
    for( let value of toolsMenu.values() ) {
        value.firstChild.className = '';
    }

    // Highlight the tool that we now have open, based on our input
    document.querySelector(nextTool).classList.add("has-text-weight-bold", "has-background-link-dark", "has-text-white");
};

/**
 * Initialises and declares event handlers for the tools in the item
 * management system.
 */
function initButtons() {
    // We check if null first just in case mustache shows a not authorised page instead, to prevent console errors on NPE

    // The item management home button
    if (document.querySelector('#management-home') != null) {
        document.querySelector('#management-home').addEventListener('click', () =>{
            displayHomePartial();
        });
    }

    // The Search Button
    if (document.querySelector('#search-tool') != null ) {
        document.querySelector('#search-tool').addEventListener('click', () => {
            displaySearchPartial();
        })
    }

    // The Item Creation Button
    if (document.querySelector('#create-tool') != null) {
        document.querySelector('#create-tool').addEventListener('click', () => {
            displayCreationPartial();
        })
    }

    // The All Items Button
    if (document.querySelector('#all-items-tool') != null) {
        document.querySelector('#all-items-tool').addEventListener('click', () => {
            displayAllItemsPartial();
        })
    }

    // The Sold Out Items Button
    if (document.querySelector('#sold-out-tool') != null) {
        document.querySelector('#sold-out-tool').addEventListener('click', () => {
            displaySoldOutPartial();
        })
    }

    // The Discontinued Items Button
    if (document.querySelector('#discontinued-items-tool') != null) {
        document.querySelector('#discontinued-items-tool').addEventListener('click', () => {
            displayDiscontinuedPartial();
        })
    }
};

/**
 * Helper function for item management.
 * Displays a template in the view, programmically
 * @param {Object} template - The template to display
 * @param {String} name - The name to display on the template header
 * @param {Object} callback - The callback to execute after we have completed our task
 */
function displayTemplate( template, name, callback ) {

    // Generate the output of our template
    let templateOutput = template({
        ToolName: name
    });

    // Clear our current view
    partialRoot.innerHTML = '';

    // Insert it into the document
    partialRoot.insertAdjacentHTML('beforeend', templateOutput);

    // Execute our callback
    callback();
}

/**
 * Helper function for item management.
 * Initialises the buttons and their functions.
 */

function itemManagementPageController() {

    let callback = () => {
        displayHomePartial();
        initButtons();
    };

    App.loadPage( 'Item Management | GlobalOne', template, {}, callback );
};

// Exports ------------------------------------------------------------------ /
export { itemManagementPageController };