/**
 * order.js
 * 
 * The order system and page controller for the SPA
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 */

// Imports ------------------------------------------------------------------ /
import { App } from '../components/app.js';                 // The core SPA app script

// Import assets for webpack
const template = require('../views/order.mustache');        // Mustache Template

// Functions ---------------------------------------------------------------- /

// TODO - Complete order system functionality

function orderPageController() {
    App.loadPage( 'Order | GlobalOne', template, {
        Authenticated: App.authorisation.authenticated
    });
};

// Exports ------------------------------------------------------------------ /
export { orderPageController };