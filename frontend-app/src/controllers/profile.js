/**
 * profile.js
 * 
 * The profile page controller for the SPA
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 */

// Imports ------------------------------------------------------------------ /
import { App } from '../components/app.js';                 // The core SPA app script
import { Account } from '../components/account.js';         // The account system component
import { Modal } from '../components/modal.js';             // The modal system component

// Import assets for webpack
const template = require('../views/profile.mustache');      // Mustache template

// Functions ---------------------------------------------------------------- /

/**
 * Initialise Buttons in the system by determining if they exist (
 * in the event that not all buttons are active for the user with
 * their given permission level).
 */
function initButtons() {
    // Order History Button
    if( document.querySelector('#order-history') != null ) {
        document.querySelector('#order-history').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'Order history is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // Marketing Settings Button
    if (document.querySelector('#marketing-settings') != null) {
        document.querySelector('#marketing-settings').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'Marketing settings configuration is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // Update Profile
    if (document.querySelector('#profile-update') != null) {
        document.querySelector('#profile-update').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The profile update feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // Password Change
    if (document.querySelector('#password-change') != null) {
        document.querySelector('#password-change').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The Password Change feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // Shift Management
    if (document.querySelector('#shift-management') != null) {
        document.querySelector('#shift-management').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The Shift Management feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // POS System
    if (document.querySelector('#pos-system') != null) {
        document.querySelector('#pos-system').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The POS System is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // Item Management
    if (document.querySelector('#item-management') != null) {
        document.querySelector('#item-management').addEventListener('click', () => {
            location.hash = '#manage-items'
        });
    }

    // Restaurant Management
    if (document.querySelector('#restaurant-management') != null) {
        document.querySelector('#restaurant-management').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The Restaurant Management feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // Staff Management
    if (document.querySelector('#staff-management') != null) {
        document.querySelector('#staff-management').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The Staff Management feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // System Management
    if (document.querySelector('#system-management') != null) {
        document.querySelector('#system-management').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The System Management feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // User Management
    if (document.querySelector('#user-management') != null) {
        document.querySelector('#user-management').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The User Management feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }

    // Website Management
    if (document.querySelector('#website-management') != null) {
        document.querySelector('#website-management').addEventListener('click', () => {
            Modal.displayWarning(
                'Not yet implemented',
                'The Website Management feature is not yet implemented. Please check back later',
                'Okay'
            );
            Modal.setPrimaryAsClose();
        });
    }
}

/**
 * Callback for the profilePageContorller function that
 * initialises various features and event listeners
 */
function callback() {
    initButtons();
}

function profilePageController() {

    let isStaff = Account.permissions.includes(2);
    let isManagement = Account.permissions.includes(5);

    // For debugging
    // console.log(Account.permissions);
    // console.log(isStaff, isManagement);

    App.loadPage( 'My Profile | GlobalOne', template, {
        // Check the App's Auth Object if we are authenticated
        Authenticated: App.authorisation.authenticated,
        StaffPermissions: isStaff,
        ManagementPermissions: isManagement,
        UserName: Account.name
    }, callback);
};

// Exports ------------------------------------------------------------------ /
export { profilePageController };