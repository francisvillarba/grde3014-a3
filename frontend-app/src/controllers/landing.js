/**
 * landing.js
 * 
 * The index / landing page controller for the SPA
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 */

// Imports ------------------------------------------------------------------ /
import { App } from '../components/app.js';             // The core SPA app script

// Import assets for webpack
const template = require('../views/landing.mustache');      // Mustache Template
import companyLogo from '../img/logo.png';                  // Logo Image

// Functions ---------------------------------------------------------------- /
function landingPageController() {
    App.loadPage( 'GlobalOne', template, {
        who_image: companyLogo
    });
};

// Exports ------------------------------------------------------------------ /
export { landingPageController };