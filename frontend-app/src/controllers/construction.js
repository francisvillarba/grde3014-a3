/**
 * construction.js
 * 
 * The page that shows when the user visits a section that is currently 
 * under construction in the SPA.
 * 
 * For the most part, this page should only be displayed in the event
 * that the user goes to a section of the site that is not within the
 * scope of the first design iteration, as outlined in the project
 * specifications document in GRDE3014, Web Authoring Design,
 * Assignment 2.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.0
 */

// Imports ------------------------------------------------------------------ /
import { App } from '../components/app.js';                         // The core SPA app script

// Import assets for webpack to include
const template = require('../views/construction.mustache');         // Mustache Template for Mustache-Loader

// Functions ---------------------------------------------------------------- /

/**
 * Helper function that converts the hash into a version that best suits
 * the role of being used in a title
 */
function getCurrentHash() {
    let hash = location.hash || '#';
    let subString = hash.substr(1);
    return subString.charAt(0).toUpperCase() + subString.substr(1);
}

/**
 * The function that router will call, which executes the core app SPA's
 * loadPage method to compile and show the under construction page
 */
function constructionPageController() {
    App.loadPage( 'Under Construction | GlobalOne', template, {
        page: getCurrentHash()
    });
};

// Exports ------------------------------------------------------------------ /
export { constructionPageController };