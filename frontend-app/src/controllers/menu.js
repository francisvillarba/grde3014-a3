/**
 * menu.js
 * 
 * Menu page and controller for the SPA
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.1.3
 */

// Imports ------------------------------------------------------------------ /

// SPA Components
import { App } from '../components/app.js';                     // The core SPA system component
import { Menu } from '../components/menu.js';                   // The menu system component

// Import assets for webpack
const template = require('../views/menu.mustache');             // The menu page mustache template


// Functions ---------------------------------------------------------------- /

/**
 * Helper function for the menu page controller
 * Initialises the menu / filter buttons
 */
function initMenuButtons() {
    // Menu Types ----------------------------------------------------------- /

    // All menu button
    document.querySelector('#menu-all').addEventListener('click', () => {
        // Set the parameters
        Menu.currentMenu = 'menu-all';
        Menu.currentFilter = 'none';
        // Reflect the currently selected menu
        updateSelection();
        // Update the Menu itself
        Menu.updateMenu();
        // Update the header text
        document.querySelector('#current-filter-text').innerHTML = 'All Items (A-Z)';
    });

    // Lunch menu button
    document.querySelector('#menu-lunch').addEventListener('click', () => {
        Menu.currentMenu = 'menu-lunch';
        Menu.currentFilter = 'none';
        updateSelection();
        Menu.updateMenu();
        document.querySelector('#current-filter-text').innerHTML = 'Lunch Menu';
    });

    // Dinner menu button
    document.querySelector('#menu-dinner').addEventListener('click', () => {
        Menu.currentMenu = 'menu-dinner';
        Menu.currentFilter = 'none';
        updateSelection();
        Menu.updateMenu();
        document.querySelector('#current-filter-text').innerHTML = 'Dinner Menu';
    });

    // Filters -------------------------------------------------------------- /

    // Dim Sum
    document.querySelector('#filter-dimsum').addEventListener('click', () => {
        Menu.currentFilter = 'filter-dimsum';
        updateSelection();
        Menu.updateMenu();
        // Determine what to show on the UI
        if( Menu.currentMenu == 'menu-all' ) {
            document.querySelector('#current-filter-text').innerHTML = 'All Dim Sum Items (A-Z)';
        } else if ( Menu.currentMenu == 'menu-lunch' ) {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Dim Sum';
        } else if ( Menu.currentMenu == 'menu-dinner' ) {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Dim Sum';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'Dim Sim';
        }
    });

    //  BBQ
    document.querySelector('#filter-bbq').addEventListener('click', () => {
        Menu.currentFilter = 'filter-bbq';
        updateSelection();
        Menu.updateMenu();
        // Determine what to show on the UI
        if( Menu.currentMenu == 'menu-all' ) {
            document.querySelector('#current-filter-text').innerHTML = 'All Barbeque Items (A-Z)';
        } else if ( Menu.currentMenu == 'menu-lunch' ) {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Barbeque';
        } else if ( Menu.currentMenu == 'menu-dinner' ) {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Barbeque';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'Barbeque';
        }
    });

    //  Noodles
    document.querySelector('#filter-noodles').addEventListener('click', () => {
        Menu.currentFilter = 'filter-noodles';
        updateSelection();
        Menu.updateMenu();
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Noodle Items (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Noodles';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Noodles';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'Noodles';
        }
    });

    //  Rice
    document.querySelector('#filter-rice').addEventListener('click', () => {
        Menu.currentFilter = 'filter-rice';
        updateSelection();
        Menu.updateMenu();
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Rice Items (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Rice';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Rice';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'Rice';
        }
    });

    //  Sides
    document.querySelector('#filter-sides').addEventListener('click', () => {
        Menu.currentFilter = 'filter-sides';
        updateSelection();
        Menu.updateMenu();
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Side Items (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Sides';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Sides';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'Sides';
        }
    });

    //  Deserts
    document.querySelector('#filter-deserts').addEventListener('click', () => {
        Menu.currentFilter = 'filter-deserts';
        updateSelection();
        Menu.updateMenu();
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Desert Items (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Deserts';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Deserts';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'Deserts';
        }
    });

    // Beverages  ----------------------------------------------------------- /
    
    // All Drinks Button
    document.querySelector('#filter-drinks').addEventListener('click', () => {
        // Set the parameters
        Menu.currentFilter = 'filter-drinks';
        // Reflect the currently selected menu
        updateSelection();
        // Update the Menu itself
        Menu.updateMenu();
        // Update the header text
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Drink Items (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'All Lunch Drinks';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'All Dinner Drinks';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'All Drinks';
        }
    });

    // Teas
    document.querySelector('#filter-tea').addEventListener('click', () => {
        // Set the parameters
        Menu.currentFilter = 'filter-tea';
        // Reflect the currently selected menu
        updateSelection();
        // Update the Menu itself
        Menu.updateMenu();
        // Update the header text
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Tea Items (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Tea Drinks';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Tea Drinks';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'All Teas';
        }
    });

    // Soft Drinks
    document.querySelector('#filter-softdrinks').addEventListener('click', () => {
        // Set the parameters
        Menu.currentFilter = 'filter-softdrinks';
        // Reflect the currently selected menu
        updateSelection();
        // Update the Menu itself
        Menu.updateMenu();
        // Update the header text
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Soft Drinks (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Soft Drinks';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Soft Drinks';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'All Soft Drinks';
        }
    });

    // Alcohol
    document.querySelector('#filter-alcohol').addEventListener('click', () => {
        // Set the parameters
        Menu.currentFilter = 'filter-alcohol';
        // Reflect the currently selected menu
        updateSelection();
        // Update the Menu itself
        Menu.updateMenu();
        // Update the header text
        // Determine what to show on the UI
        if (Menu.currentMenu == 'menu-all') {
            document.querySelector('#current-filter-text').innerHTML = 'All Alcoholic Items (A-Z)';
        } else if (Menu.currentMenu == 'menu-lunch') {
            document.querySelector('#current-filter-text').innerHTML = 'Lunch Alcohol Drinks';
        } else if (Menu.currentMenu == 'menu-dinner') {
            document.querySelector('#current-filter-text').innerHTML = 'Dinner Alcohol Drinks';
        } else {
            // Specials
            document.querySelector('#current-filter-text').innerHTML = 'All Alcoholic Drinks';
        }
    });
};

/**
 * Helper function for the menu page controller
 * Scans through the filters and menu selection and updates it to the currently
 * selected option.
 */
function updateSelection() {
    // For debugging
    // console.log('Update Selection');

    // Get all the menu selectors
    let menuSelection = document.querySelectorAll('#menu-selection > li');
    // For each element in the list, clear their classlist
    for (let value of menuSelection.values()) {
        // For debugging
        // console.log(value.firstChild);
        value.firstChild.className = '';
    };

    // Get all Food Category Selectors
    let categorySelection = document.querySelectorAll('#menu-categories > li');
    // For each element in the list, clear their classlist
    for (let value of categorySelection.values()) {
        // For debugging
        // console.log(value.firstChild);
        value.firstChild.className = '';
    };

    // Get all beverage category selectors
    let beverageSelection = document.querySelectorAll('#menu-beverages > li');
    // For each element in the list, clear their classlist
    for (let value of beverageSelection.values()) {
        // For debugging
        // console.log(value.firstChild);
        value.firstChild.className = '';
    };

    // Update the selection(s) to the current one selected
    document.querySelector(`#${Menu.currentMenu}`).classList.add("has-text-weight-bold", "has-background-link-dark", "has-text-white");
    if( document.querySelector(`#${Menu.currentFilter}`) != null ) {
        // Only do this if we can actually select something
        document.querySelector(`#${Menu.currentFilter}`).classList.add("has-text-weight-bold", "has-background-link-dark", "has-text-white");
    }
};

/**
 * The page controller for the menu sytsem
 */
function menuPageController() {

    let callback = () => {
        // Call the meny component to start doing it's thing
        Menu.init();
        initMenuButtons();
    };

    App.loadPage( 'Menu | GlobalOne', template, {}, callback );
};

// Exports ------------------------------------------------------------------ /
export { menuPageController };