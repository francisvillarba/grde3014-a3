/**
 * menu.js
 * 
 * The menu system component for the SPA
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 2.1.3
 */

// Imports ------------------------------------------------------------------ /
// SPA Components
import { App } from '../components/app.js';                     // The core SPA system component
import { Item } from '../components/item.js';                   // The item system component
import { Modal } from '../components/modal.js';                 // The modal system component

// Node Modules
import anime from 'animejs/lib/anime.es.js'; // Anime library

// Import assets for webpack
const itemTemplate = require('../partials/menu_item.mustache'); // Menu Item Partial Template
const itemDetailTemplate = require('../partials/menu_item_detail.mustache'); // Menu Item Detail Partial Template

// Functions ---------------------------------------------------------------- /
const Menu = {
    // Object Parameters
    currentMenu: 'menu-all',
    currentFilter: 'none',

    /**
     * Initialises the menu view
     */
    init: () => {
        Menu.generateInitialMenu();
        // Remove the menu loading circle graphic
        document.querySelector('#loading').remove();
    },

    /**
     * Generates and adds the menu item to the page to be displayed to by the
     * user. This particular method does not include the price of the item
     * and is geared more toward the general (initial) menu view.
     */
    generateAndDisplayItemNP: (object) => {
        // For debugging
        // console.log(object);

        // Set the root menu element
        let menuRoot = document.querySelector('#menu-item-grid');

        // Generate the menu item
        let itemOutput = itemTemplate({
            ItemID: object._id,
            ShowPrice: false,
            ImageURL: object.thumbnail,
            ImageAlt: `${object.name} thumbnail image`,
            ItemName: object.name
        });

        // Insert the generated output into the page
        // Use insertAdjacentHTML to ensure it is inserted as HTML not as string
        menuRoot.insertAdjacentHTML('beforeend', itemOutput);

        // Assign interactivity elements to the menu item (click to open) using the Object_Id as the ID tag
        // Use CSS Escape to allow our selector to work with IDs that start with numbers
        document.querySelector(`#${CSS.escape(object._id)}`).addEventListener('click', function () {
            // Generate the menu item detail
            let itemDetailOutput = itemDetailTemplate({
                ImageURL: object.image,
                ImageAlt: `${object.name} Image`,
                ItemName: `${object.name}`,
                ItemDescription: `${object.description}`
            });

            // Display a modal with that item detail
            Modal.displayDocument(
                `${object.name}`,
                itemDetailOutput,
                'Close'
            );
            // Set the primary action button to close the modal
            Modal.setPrimaryAsClose();
        });
    },

    /**
     * Generates and adds the menu item to the page to be displayed to by the
     * user. This particular method shows the price of the item, useful
     * for displaying the lunch or dinner time price.
     */
    generateAndDisplayItem: (object) => {
        // Figure out the price to display
        let price = '';
        // Determine which value to use based on menu type or specials
        // Use numberdecimal as our final item call as that is the datatype that mongoDB stores Decimal128 (doubles) as
        if (object.isOnSpecial) {
            // The item is on special
            price = `$ ${object.price.special.$numberDecimal}`;
        } else if (Menu.currentMenu == '#menu-lunch') {
            // Lunch price
            price = `$ ${object.price.lunch.$numberDecimal}`;
        } else {
            // Dinner Price
            price = `$ ${object.price.dinner.$numberDecimal}`;
        }

        // TODO FUTURE - Ensure the prices have two decimal places.

        // Generate the menu item
        let itemOutput = itemTemplate({
            ItemID: object._id,
            ShowPrice: true,
            ImageURL: object.thumbnail,
            ImageAlt: `${object.name} thumbnail image`,
            ItemName: object.name,
            ItemPrice: price
        });

        // Define where the items go
        let menuRoot = document.querySelector('#menu-item-grid');
        // Insert the generated output into the page
        menuRoot.insertAdjacentHTML('beforeend', itemOutput);

        // Use CSS Escape to allow our selector to work on IDs that start with numbers
        document.querySelector(`#${CSS.escape(object._id)}`).addEventListener('click', function () {
            // Generate the menu item detail
            let itemDetailOutput = itemDetailTemplate({
                ImageURL: object.image,
                ImageAlt: `${object.name} Image`,
                ItemName: `${object.name}`,
                ItemDescription: `${object.description}`,
                ItemPrice: price
            });

            // Display a modal with that item detail
            Modal.displayDocument(
                `${object.name}`,
                itemDetailOutput,
                'Close'
            );
            // Set the primary action button to close the modal
            Modal.setPrimaryAsClose();
        });
    },

    /**
     * Helper method that is used to show menu items after loading in a slick
     * animated fashion. Uses anime and stagger delay per item for that
     * extra OMPH!
     */
    showItems: () => {
        anime({
            targets: '.card',
            easing: 'easeInOutSine',
            opacity: 1,
            duration: 500,
            delay: anime.stagger(100)
        });
    },

    /**
     * Clears the menu, in preparation of displaying a new menu
     */
    clearMenu: () => {
        document.querySelector('#menu-item-grid').innerHTML = '';
    },

    /**
     * Changes the menu to the new one selected
     */
    updateMenu: () => {
        // Our Filters
        let menu = '';
        let category = '';

        // Setup our menu filter
        switch (Menu.currentMenu) {
            case 'menu-all':
                menu = 'all'
                break
            case 'menu-lunch':
                menu = 'lunch'
                break
            case 'menu-dinner':
                menu = 'dinner'
                break
        };

        // Setup our category filter
        switch (Menu.currentFilter) {
            case 'none':
                category = ''
                break
            case 'filter-dimsum':
                category = 'Dim%20Sum'
                break
            case 'filter-bbq':
                category = 'BBQ'
                break
            case 'filter-noodles':
                category = 'Noodles'
                break
            case 'filter-rice':
                category = 'Rice'
                break
            case 'filter-sides':
                category = 'Sides'
                break
            case 'filter-deserts':
                category = 'Deserts'
                break
            case 'filter-drinks':
                category = 'Beverage'
                break
            case 'filter-tea':
                category = 'Tea'
                break
            case 'filter-softdrinks':
                category = 'Soft%20Drink'
                break
            case 'filter-alcohol':
                category = 'Alcohol'
                break
        };

        // For debugging
        // console.log( menu, category );

        // Clear the current menu
        Menu.clearMenu();

        if( menu == 'all' && category == '' ) {
            Menu.generateInitialMenu();
        }
        else if( category == '' ) {
            // If we don't have a category filter
            Menu.generateMenuByType(menu);
        } else {
            // For debugging
            // console.log('Filter on both menu and category');

            // Have both menu and category filter to do
            Menu.generateMenuByFilters( menu, category );
        }

        // Show the menu items
        Menu.showItems();
    },

    /**
     * Generates the initial menu that shows all items (A-Z)
     * without drinks
     */
    generateInitialMenu: () => {
        // Get all items from the database (initially A-Z All items node)
        Item.getItems()
        .then((res) => {
            // Do error handling checks first before doing stuff
            if (!res) {
                // If we got nothing back, display an error in both console and UI
                console.log(`[Menu] Error! - Empty response given by server`);
                Modal.displayError(
                    'Menu System Error',
                    'The server could not complete the request at this time. Please try again later',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else if (res == 'TypeError: Failed to fetch') {
                // The server was unable to respond (network error?)
                console.log(`[Menu] Error! - Unable to communicate with the server`);
                Modal.displayError(
                    'Menu System Error',
                    'Could not communicate with the server. Please check your internet connectivity then try again',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else if (res == '404') {
                // The items collection is empty for some reason :O
                console.log(`[Menu] Warning! - The menu is empty!`);
                Modal.displayWarning(
                    'Menu System Error',
                    'The server returned an empty menu. There is a chance that the menu is in the process of being updated. Please try again later',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else {
                // Okay, let's check the json response to see if we have what we want
                if (res.hasOwnProperty('msg')) {
                    // Well the server wasn't able to do what we want
                    console.log(`[Menu] Error! - The server was unable to complete the request`);
                    Modal.displayWarning(
                        'Server Response',
                        `The server responded "${res.msg}" which was not expected. Please try again later`,
                        'Okay'
                    );
                    Modal.removePrimaryAction();
                } else {
                    // All is well, let's parse the menu items

                    // Parse the menu items and display only drinks in the all items a-z menu
                    // We display food only as drinks is seprate menu (AND!) people care about food first
                    for (let x in res) {
                        if (!res[x].tags.includes('Beverage') && Menu.currentFilter != 'Beverage' ) {
                            // Pass it to our helper method to generate and display the item
                            Menu.generateAndDisplayItemNP(res[x]);
                        }
                    }
                    // Animate the menu items (so they nicely load in and display)
                    Menu.showItems();
                }
            }
        })
        .catch((err) => {
            // An error occured while trying to get items, log and display error to UI
            console.log(err);
            Modal.displayError(
                'Menu System Error',
                err,
                'Okay'
            );
            Modal.removePrimaryAction();
        });
    },

    /**
     * Generates a menu based on a given menu type
     */
    generateMenuByType: (type) => {
        Item.getItemsByAvailability(type)
        .then((res) => {
            // Do error handling checks first before doing stuff
            if (!res) {
                // If we got nothing back, display an error in both console and UI
                console.log(`[Menu] Error! - Empty response given by server`);
                Modal.displayError(
                    'Menu System Error',
                    'The server could not complete the request at this time. Please try again later',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else if (res == 'TypeError: Failed to fetch') {
                // The server was unable to respond (network error?)
                console.log(`[Menu] Error! - Unable to communicate with the server`);
                Modal.displayError(
                    'Menu System Error',
                    'Could not communicate with the server. Please check your internet connectivity then try again',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else if (res == '404') {
                // The items collection is empty for some reason :O
                console.log(`[Menu] Warning! - The menu is empty!`);
                Modal.displayWarning(
                    'Menu System Error',
                    'The server returned an empty menu. There is a chance that the menu is in the process of being updated. Please try again later',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else {
                // Okay, let's check the json response to see if we have what we want
                if (res.hasOwnProperty('msg')) {
                    // Well the server wasn't able to do what we want
                    console.log(`[Menu] Error! - The server was unable to complete the request`);
                    Modal.displayWarning(
                        'Server Response',
                        `The server responded "${res.msg}" which was not expected. Please try again later`,
                        'Okay'
                    );
                    Modal.removePrimaryAction();
                } else {
                    // All is well, let's parse the menu items
                    // Parse the menu items and display only drinks in the all items a-z menu
                    for (let x in res) {
                        if ( !res[x].tags.includes('Beverage') ) {
                            // Pass it to our helper method to generate and display the item
                            Menu.generateAndDisplayItem(res[x]);
                        }
                    }

                    // Animate the menu items (so they nicely load in and display)
                    Menu.showItems();
                }
            }
        })
        .catch((err) => {
            // An error occured while trying to get items, log and display error to UI
            console.log(err);
            Modal.displayError(
                'Menu System Error',
                err,
                'Okay'
            );
            Modal.removePrimaryAction();
        });
    },

    /**
     * Generates a menu based on type and category filters
     */
    generateMenuByFilters: (menu, filter) => {
        Item.getItemsFiltered(menu, filter)
        .then((res) => {
            // Do error handling checks first before doing stuff
            if (!res) {
                // If we got nothing back, display an error in both console and UI
                console.log(`[Menu] Error! - Empty response given by server`);
                Modal.displayError(
                    'Menu System Error',
                    'The server could not complete the request at this time. Please try again later',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else if (res == 'TypeError: Failed to fetch') {
                // The server was unable to respond (network error?)
                console.log(`[Menu] Error! - Unable to communicate with the server`);
                Modal.displayError(
                    'Menu System Error',
                    'Could not communicate with the server. Please check your internet connectivity then try again',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else if (res == '404') {
                // The items collection is empty for some reason :O
                console.log(`[Menu] Warning! - The menu is empty!`);
                Modal.displayWarning(
                    'Menu System Error',
                    'The server returned an empty menu. There is a chance that the menu is in the process of being updated. Please try again later',
                    'Okay'
                );
                Modal.removePrimaryAction();

            } else {
                // Okay, let's check the json response to see if we have what we want
                if (res.hasOwnProperty('msg')) {
                    // Well the server wasn't able to do what we want
                    console.log(`[Menu] Error! - The server was unable to complete the request`);
                    Modal.displayWarning(
                        'Server Response',
                        `The server responded "${res.msg}" which was not expected. Please try again later`,
                        'Okay'
                    );
                    Modal.removePrimaryAction();
                } else {
                    // All is well, let's parse the menu items
                    // Parse the menu items and display only drinks in the all items a-z menu
                    // console.log('Rendering item time');
                    for (let x in res) {
                        if( Menu.currentMenu == 'menu-all' ) {
                            // Don't display the price
                            if( !res[x].tags.includes('Beverage')) {
                                Menu.generateAndDisplayItemNP(res[x]);
                            } else {
                                // We determine if we really do need to add this item (drinks)
                                // console.log('currentFilter is', Menu.currentFilter);
                                if( Menu.currentFilter == 'filter-drinks' ) { Menu.generateAndDisplayItemNP(res[x]); }
                                if( Menu.currentFilter == 'filter-tea' ) { Menu.generateAndDisplayItemNP(res[x]); }
                                if( Menu.currentFilter == 'filter-softdrinks' ) { Menu.generateAndDisplayItemNP(res[x]); }
                                if( Menu.currentFilter == 'filter-alcohol' ) { Menu.generateAndDisplayItemNP(res[x]); }
                            }
                        }
                        else if(!res[x].tags.includes('Beverage')) {
                            // Pass it to our helper method to generate and display the item
                            Menu.generateAndDisplayItem(res[x]);
                        } else {
                            // We determine if we really do need to add this item
                            // console.log('currentFilter is', Menu.currentFilter);
                            if( Menu.currentFilter == 'filter-drinks' ) { Menu.generateAndDisplayItem(res[x]); }
                            if( Menu.currentFilter == 'filter-tea' ) { Menu.generateAndDisplayItem(res[x]); }
                            if( Menu.currentFilter == 'filter-softdrinks' ) { Menu.generateAndDisplayItem(res[x]); }
                            if( Menu.currentFilter == 'filter-alcohol' ) { Menu.generateAndDisplayItem(res[x]); }
                        }
                    }

                    // Animate the menu items (so they nicely load in and display)
                    Menu.showItems();
                }
            }
        })
        .catch((err) => {
            // An error occured while trying to get items, log and display error to UI
            console.log(err);
            Modal.displayError(
                'Menu System Error',
                err,
                'Okay'
            );
            Modal.removePrimaryAction();
        });
    }
};

// Exports ------------------------------------------------------------------ /
export { Menu };