/**
 * account.js
 * 
 * An auxiliary component for the GlobalOne Restaurant System.
 * The purpose of this component is to handle user accounts, including
 * the generation of a user, updating / editing of user information as
 * well as fetching user information from the database and deletion
 * / disabling of a user's account.
 * 
 * Inspired by GRDE3014, Web Authoring Design, Weekly workshop content by
 * Daniel Brouse, with changes made for my specific needs for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.3
 */

// Imports ------------------------------------------------------------------ /
import { App } from './app.js';                     // The core SPA system component
import { Auth } from './auth.js';                   // The authentication system component

// Functionality ------------------------------------------------------------ /
const Account = {
    userID: '',
    name: '',
    permissions: [],

    /**
     * Initialises the account system
     */
    init: (userID) => {
        // Input our user ID
        Account.userID = userID;

        // Attempt to get our permissions
        fetch( `http://localhost:8081/api/users/${Account.userID}`, {
            method: 'GET',
            headers: new Headers({
                'Authorization': Auth.token
            })
        })
        .then( (res) => {
            // For debugging
            // console.log(res);
            res.json().then( (jsonRes) => {
                // console.log(jsonRes);

                // Set the permissions
                Account.permissions = jsonRes.privileges;
                Account.name = jsonRes.name;
                // console.log(Account.name);
                // console.log(Account.permissions);
            });
        })
        .catch( (err) => {
            // An error occured while getting our user account and permissions
            console.log(err);
        });
    },
};

// Exports ------------------------------------------------------------------ /
export { Account };