/**
 * item.js
 * 
 * A core system component for the GlobalOne Restaurant System.
 * 
 * The purpose of this component is to handle items, including the
 * creation / generation of an item, updating and editing item
 * information, fetching item information and deletion of 
 * an item from the database.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.5
 */

// Imports ------------------------------------------------------------------ /

import { App } from './app.js';                         // The core SPA system component

// Functionality ------------------------------------------------------------ /

// TODO - Implement the item functionality
const Item = {
    
    /**
     * Create a new item
     */
    create: (formData) => {
        // TODO - Implement this stub
    },

    /**
     * Update an item
     */
    update: (formData) => {
        // TODO - Implement this stub
    },

    /**
     * Delete an item (Will reject deletion if the item has been purchased at least once)
     */
    delete: (id) => {
        // TODO - Implement this stub
    },

    /**
     * Get all information about a single item
     */
    getItem: (id) => {
        // TODO - Implement this stub
    },

    /**
     * Get all items from the database (no filter)
     */
    getItems: () => {
        // For debugging
        // console.log( `[Item] Getting all Items from the database` );

        let result = fetch('http://localhost:8081/api/items', { method: 'GET' })
        .then( (res) => {
            if( res.status == 404 ) {
                // The items collection is empty, that's odd
                // console.log('The items collection is empty!');
                return res.status;
            }
            else if( res.status != 200 ) {
                // If the server did not respond with an ok, return the status to the caller
                return res.status;
            } else {
                // Otherwise, convert response to json and return to the caller
                return res.json();
            }
        })
        .catch( (err) => {
            // A problem occured when trying to get items
            console.log(`Error occured while communicating with the database`);
            console.log(err);
            console.log(err.kind);
            // Return the error to the caller
            return err;
        });
        // Return the result of the fetch to the caller
        return result;
    },

    /**
     * Get items based on their availability
     */
    getItemsByAvailability: (filter) => {
        let result = fetch(`http://localhost:8081/api/items/menu/${filter}`, { method: 'GET' })
        .then( (res) => {
            if (res.status == 404) {
                // The items collection is empty, that's odd
                // console.log('The items collection is empty!');
                return res.status;
            } else if (res.status != 200) {
                // If the server did not respond with an ok, return the status to the caller
                return res.status;
            } else {
                // Otherwise, convert response to json and return to the caller
                return res.json();
            }
        })
        .catch((err) => {
            // A problem occured when trying to get items
            console.log(`Error occured while communicating with the database`);
            console.log(err);
            console.log(err.kind);
            // Return the error to the caller
            return err;
        });
        // Return the result of the fetch to the caller
        return result;
    },

    /**
     * Get items based on their availability and filters
     */
    getItemsFiltered: (menu, filter) => {
        let result = fetch(`http://localhost:8081/api/items/menu/${menu}/${filter}`, { method: 'GET' })
        .then( (res) => {
            if (res.status == 404) {
                // The items collection is empty, that's odd
                // console.log('The items collection is empty!');
                return res.status;
            } else if (res.status != 200) {
                // If the server did not respond with an ok, return the status to the caller
                return res.status;
            } else {
                // Otherwise, convert response to json and return to the caller
                return res.json();
            }
        })
        .catch((err) => {
            // A problem occured when trying to get items
            console.log(`Error occured while communicating with the database`);
            console.log(err);
            console.log(err.kind);
            // Return the error to the caller
            return err;
        });
        // Return the result of the fetch to the caller
        return result;
    }
};

// Exports ------------------------------------------------------------------ /
export { Item };

// Notes to self
// On Search Event https://www.w3schools.com/tags/ev_onsearch.asp
// Search AutoComplete https://www.w3schools.com/howto/howto_js_autocomplete.asp
// Form OnChange (When field is tabbed or completed) https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_ev_onchange

// Other Notes
// All HTML events https://www.w3schools.com/tags/ref_eventattributes.asp
// Font Awesome https://fontawesome.com/icons?d=gallery

// Menu Item Display Ideas
// https://bulma.io/documentation/components/card/
// https://bulma.io/documentation/columns/basics/