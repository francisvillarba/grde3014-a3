/**
 * app.js
 * 
 * The main / primary application javascript for the GlobalOne Restaurant System.
 * Created for GRDE3014, Web Authoring Design, Assignment 3. A single page application 
 * concept, without the use of client-side JavaScript frameworks.
 * 
 * Inspired by GRDE3014, Web Authoring Design, Weekly workshop content by
 * Daniel Brouse with some changes made for my specific needs for
 * Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.3 
 */

// Imports ------------------------------------------------------------------ /

require('dotenv').config();                         // Configuration file

// Import assets for webpack to function
import companyLogo from '../img/logo_wide.png';
const navigationBarTemplate = require( '../partials/navigation.mustache');
const footerBarTemplate = require( '../partials/footer.mustache');

// Other components of our app SPA
import { Auth } from './auth.js';                   // Authentication System Component
import { Modal } from './modal.js';                 // Modal System Component
import { Account } from './account.js';             // The account system component

// Functionality ------------------------------------------------------------ /

// Declaring the app object
const App = {
    // Application Properties (mainly used for our greeting)
    name: process.env.APP_NAME,
    version: process.env.APP_VERSION,
    author: process.env.APP_AUTHOR,

    // Application runtime variables (and objects)
    appRoot: document.querySelector( "#app" ),      // Where does our dynamic content go?
    appLoader: document.querySelector( "#loader" ), // Where is the loading animation?
    routes: {},                                     // Routes for the application
    previousRoute: '#',                             // The previous page the user was on
    authorisation: Auth,                            // The authorisation object

    // Methods -------------------------------------------------------------- /

    /**
     * Initialises the single page application
     */
    init: () => {
        // Greetings!
        App.greetConsole();
        // Initialise the authorisation object
        App.authorisation.init();
        // Run the router method
        App.router();
        // Run router whenever the URL changes
        window.addEventListener( "hashchange", (event) => {
            // Set the previous URL
            App.previousRoute = '#' + event.oldURL.split('#')[1];
            // Tell router to do it's thing!
            App.router();
        });
        // Show the app
        App.appRoot.classList.remove('hidden');
        // Add an event listener for us to show and hide the loading whenever the document changes ready to loading state
        document.addEventListener( 'readystatechange', () => {
            // console.log( `SPA loading is: ${document.readyState}` );
            // If loading, show the loading overlay
            if (document.readyState === 'loading') {
                App.appLoader.classList.remove('hidden');
            }
            // If done, hide the loading overlay
            if( document.readyState === 'complete' ) {
                // Hide the pre-loader animation
                App.appLoader.classList.add('hidden');
            }
        });
    },

    /**
     * Adds a route to the single page application for us to handle
     * @param hashPash - The path that this route will handle, i.e. #login
     * @param pageController - The page controller object that we will use to execute in response
     */
    addRoute: ( hashPath, pageController ) => {
        // Add it to our routes object with the key being the hashPath
        App.routes[ hashPath ] = {
            controller: pageController
        };
    },

    /**
     * Routes the user to various parts of the single page application based on
     * the current hash of the browser window and the routes that has been
     * configured on setup
     */
    router: () => {
        // For debugging
        // console.log( location.hash );

        // Declare our variables
        let path = location.hash || '#';      // Use # if null
        let hashPath = App.routes[ path ];    // What is the user requesting to view?

        // Check if this is a valid route
        if(hashPath) {
            // If valid route, run the controller method for that route
            hashPath.controller();
        } else {
            // For debugging
            App.loadPage(
                `Error 404 | ${process.env.APP_DEFAULT_TITLE}`,
                require('../views/404.mustache'), {
                    request: path,
                    logo: companyLogo
                }
            );
        }
        // For debugging
        // console.log(`Current HashPath: ${location.hash}`);
        // console.log(`Previous HashPath: ${App.previousRoute}`);
    },

    /**
     * Initialises our navigation bar for both desktop and mobile (hamburger) formats
     */
    initNav: () => {
        let linkCount = 3;                      // Padding for the hamburger menu
        let primaryNav = document.querySelector( '#navbarPrimary' );

        // Fill in the primary navigation bar
        let navbarlinks = document.querySelector( "#navbarLinks" );
        navbarlinks.innerHTML = `
            <a class="navbar-item" href="#about">About</a>
            <a class="navbar-item" href="#gallery">Gallery</a>
            <a class="navbar-item" href="#menu">Menu</a>
            <a class="navbar-item" href="#order">Order</a>
            <a class="navbar-item" href="#contact">Contact</a>
        `;

        // For debugging
        // console.log(`Am I authenticated? ${Auth.authenticated}`);

        let navbarAccountLinks = document.querySelector("#navbarAccountLinks");
        if( Auth.authenticated ) {
            navbarAccountLinks.innerHTML = `
                <a class="button is-light" href="#profile">
                    <span class="icon"><i class="fas fa-user"></i></span>
                    <span>Profile</span>
                </a>
                <a id="logout-button" class = "button is-light">Log Out</a>
            `;
        } else {
            // Fill in account login and register links
            navbarAccountLinks.innerHTML = `
                <a class="button is-light" href="#register">Sign Up</a>
                <a class="button is-light" href="#login">Log In</a>
            `;
        }
        
        // Fill in the space for the mobile navigation bar
        let navbarMobile = document.querySelector( "#navbarMobile" );
        for( let i = 0; i < linkCount; i ++ ) {
            navbarMobile.innerHTML += `<span aria-hidden="true"></span>`;
        }

        // Link the mobile nav and primary nav data-target (so they share the same values etc)
        navbarMobile.dataset.target = "#navbarPrimary";

        // Prepare the navigation bar for mobile support
        const navbarBurgerMenu = Array.prototype.slice.call( document.querySelectorAll( '.navbar-burger' ), 0 );
        // Add hamburger menu links
        if( navbarBurgerMenu.length > 0 ) {
            // If the menu exists, add a click event to each hamburger menu item
            navbarBurgerMenu.forEach( (menuItem) => {
                menuItem.addEventListener( 'click', () => {
                    // Toggle active when interacted with
                    menuItem.classList.toggle('is-active');
                    // Toggle the main nav too, just in case the browser is resized so they are in sync
                    primaryNav.classList.toggle('is-active');
                });
            });
        };
    },

    /**
     * Updates the navigation bar to ensure that the currently selected path / link
     * is highlighted / made obvious to the user.
     */
    updateNav: () => {
        // Debug Log
        // console.log(`Update current to ${location.hash}`);

        let currentHashPath = location.hash || '#';
        let navLinks = document.querySelectorAll( '#navbarLinks > a' );
        let navAccountLinks = document.querySelectorAll( '#navbarAccountLinks > a' );

        // Iterate over the arrays to set the appropriate button as active
        // Primary Navigation Links Array
        navLinks.forEach( (link) => {
            // For debugging
            // console.log(`navLinks ${link} | ${link.getAttribute('href')}`);

            if( link.getAttribute( 'href' ) == currentHashPath ) {
                link.classList.add('has-text-weight-bold');
                link.classList.add('has-text-link');
                link.classList.add('has-background-light');
            }
        });
        // Account related navigation links array
        navAccountLinks.forEach( (link) => {
            // For debugging
            // console.log(`navAccountLinks ${link} | ${link.getAttribute('href')}`);

            if( link.getAttribute( 'href' ) == currentHashPath ) {
                if( link.innerHTML == 'Log Out' ) {
                    // If it is the logout button, we use a different highlight
                    link.classList.remove('is-light');
                    link.classList.add('is-danger');
                } else {
                    // Standard Highlight
                    link.classList.add('has-background-link');
                    link.classList.add('has-text-white');
                }
            }
        });
    },

    /**
     * Helper for the app object whereby any essential functionality / actions
     * that require additional event handling is registered here.
     * 
     * Re-registration is required on SPA page change as mustache innerHTML will
     * destroy all previous events. Not ideal especially since we are using partials.
     */
    attachEvents: () => {
        // If we have the logout button, attach the logout event to it
        if( document.querySelector('#logout-button') ) {
            // If the logout button exists, add the event for logout
            document.querySelector('#logout-button').addEventListener('click', () => {
                Auth.showSignoutConfirmation();
            });
        }
    },

    /**
     * Handles the loading and the display of the requested route to the user utilising
     * mustache, mustache-loader and webpack.
     */
    loadPage: ( title, template, data, callback ) => {
        // Set the title of the browser window
        document.title = title;

        // Add the navigation bar to the view
        let navigationOutput = navigationBarTemplate({
            logo: companyLogo
        });
        // Replace current page with the navigation menu to force page 'refresh' (when it really isn't)
        App.appRoot.innerHTML = navigationOutput;

        // Generate our view (using mustache-loader webpack plugin)
        let mustacheOutput = template(data);
        // Show the output of mustache in the app
        App.appRoot.insertAdjacentHTML('beforeend', mustacheOutput);

        // Add the footer bar to the view (a the end)
        let footerOutput = footerBarTemplate();
        // Show the output of the footer template in the app
        App.appRoot.insertAdjacentHTML('beforeend', footerOutput);

        // Initialise the navigation bar
        App.initNav();
        // Indicate the current page on the navigation bar
        App.updateNav();
        // Ensure base events are attached
        App.attachEvents();
        // Run the callback (if any)
        if( typeof callback == 'function' ) {
            // Useful if we need to run additional functions after the load, i.e. chainload the menu etc.
            callback();
        }
    },

    /**
     * Toggles the loading screen for actions that require a full screen loader
     */
    toggleLoadingScreen: () => {
        // Define the loading screen
        let loadingScreen = document.querySelector('#loader');
        
        if( loadingScreen.classList.contains('hidden') ) {
            // If presently hidden, show it
            loadingScreen.classList.remove('hidden');
        } else {
            // If currently showing, hide it
            loadingScreen.classList.add('hidden');
        }
    },

    /**
     * Outputs a small informational greeting to the user's console. Mostly for cosmetics
     * and just for fun (becuase why not? It's the little things that count!)
     */
    greetConsole: () => {
        console.info(`[App] Welcome to the ${App.name}, Version ${App.version}`);
        console.info(`[App] Created by ${App.author} for GRDE3014, Web Authoring Design, Assignment 3`);
    }
};

// Exports ------------------------------------------------------------------ /
export { App };