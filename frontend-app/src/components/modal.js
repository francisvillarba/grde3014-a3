/**
 * modal.js
 * 
 * This object deals with the creation and display of modals in the GlobalOne 
 * Restaurant System. Created for GRDE3014, Web Authoring Design, Assignment 3.
 * 
 * Inspired by GRDE3014, Web Authoring Design, Weekly workshop content by the
 * ever awesome tutor, Daniel Brouse, with changes made to suit my specific in
 * Assignemnt 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.5
 */

// Imports ------------------------------------------------------------------ /
import { App } from './app.js';                                 // The core app SPA
import anime from 'animejs/lib/anime.es.js';                // Anime library

// Import assets for webpack support
const template = require('../partials/modal.mustache');         // Mustache Template

// Functionality ------------------------------------------------------------ /

const Modal = {
    // Object Properties ---------------------------------------------------- /

    // Object Methods ------------------------------------------------------- /

    /**
     * Display a confirmation style modal window to the user
     * @param {String} title - The title of the modal
     * @param {Object} bodyContent - The body content to place within the modal body
     * @param {String} primaryAction - The primary action text
     * @param {String} secondaryAction - The secondary action text
     */
    displayConfirmation: ( title, bodyContent, primaryAction, secondaryAction ) => {
        // Generate the template
        let modalOutput = template({
            ShowClose: false,                               // Show close button
            ShowSecondary: true,                            // Show Secondary Action Button
            ModalTitle: title,                              // The title of the modal
            BodyContent: bodyContent,                       // Main Body of the Modal
            PrimaryButton: primaryAction,                   // The primary action button text
            SecondaryButton: secondaryAction                // The secondary action button text
        });

        // Append the modal to the app root element
        App.appRoot.insertAdjacentHTML('beforeend', modalOutput);

        // Display the modal
        document.querySelector('.modal').classList.add('is-active');

        // Animate the opening
        anime({
            targets: '#modal-ui',
            opacity: 0,
            keyframes: [
                { opacity: 0, duration: 0 },
                { opacity: 1, duration: 3000 }
            ]
        });
    },

    /**
     * Display a warning style modal window to the user
     * @param {String} title - The title of the modal
     * @param {Object} bodyContent - The body content to place within the modal body
     * @param {String} primaryAction - The primary action text
     */
    displayWarning: ( title, bodyContent, primaryAction ) => {        
        // Generate the template
        let modalOutput = template({
            ShowClose: false,
            ShowSecondary: false,
            ModalTitle: title,
            BodyContent: bodyContent,
            PrimaryButton: primaryAction
        });

        // Append the modal to the app root element
        App.appRoot.insertAdjacentHTML('beforeend', modalOutput);

        // Ensure the modal is coloured appropriately
        document.querySelector('.modal-card-head').classList.add('has-background-warning');

        // Display the modal
        document.querySelector('.modal').classList.add('is-active');

        // Animate the opening
        anime({
            targets: '#modal-ui',
            opacity: 0,
            keyframes: [
                { opacity: 0, duration: 0 },
                { opacity: 1, duration: 3000 }
            ]
        });
    },

    /**
     * Display an error style modal window to the user
     * @param {String} title - Title of the modal
     * @param {Object} bodyContent - The body content to place within the modal body
     * @param {String} primaryAction - The primary action text
     */
    displayError: ( title, bodyContent, primaryAction ) => {
        // Generate the template
        let modalOutput = template({
            ShowClose: false,
            ShowSecondary: false,
            ModalTitle: title,
            BodyContent: bodyContent,
            PrimaryButton: primaryAction
        });

        // Append the modal to the app root element
        App.appRoot.insertAdjacentHTML('beforeend', modalOutput);

        // Ensure the modal is coloured appropriately
        document.querySelector('.modal-card-head').classList.add('has-background-danger-dark');
        document.querySelector('.modal-card-title').classList.add('has-text-white');
        document.querySelector('.modal-card-foot').classList.add('has-background-danger-dark');
        document.querySelector('.modal-card-body').classList.add('has-background-danger-light');
        document.querySelector('#modal-ui-primary-button').classList.remove('is-success');
        document.querySelector('#modal-ui-primary-button').classList.add('is-outlined');

        // Display the modal
        document.querySelector('.modal').classList.add('is-active');

        // Animate the opening
        anime({
            targets: '#modal-ui',
            opacity: 0,
            keyframes: [
                { opacity: 0, duration: 0 },
                { opacity: 1, duration: 3000 }
            ]
        });
    },

    /**
     * Display a document style modal window to the user
     * @param {String} title - The title of the modal
     * @param {Object} bodyContent - The body content to place within the modal body
     * @param {String} primaryAction - The primary action text
     */
    displayDocument: ( title, bodyContent, primaryAction ) => {
        // Generate the template
        let modalOutput = template({
            ShowClose: false,
            ShowSecondary: false,
            ModalTitle: title,
            ShowDocument: true,
            PrimaryButton: primaryAction
        }, {
            // Partials
            Document: bodyContent
        });

        // Append the modal to the app root element
        App.appRoot.insertAdjacentHTML('beforeend', modalOutput);

        // Display the modal
        document.querySelector('.modal').classList.add('is-active');

        // Animate the opening
        anime({
            targets: '#modal-ui',
            opacity: 0,
            keyframes: [
                { opacity: 0, duration: 0 },
                { opacity: 1, duration: 3000 }
            ]
        });
    },

    /**
     * Displays a standard modal window to the user
     * @param {String} title - The title of the modal window
     * @param {Object} bodyContent - The body content to place within the modal body
     * @param {Boolean} showClose - Show the close button?
     * @param {Boolean} showSecondary - Show the secondary action button?
     * @param {String} primaryAction - The primary action text
     * @param {String} secondaryAction - The secondary action text
     */
    displayModal: ( title, bodyContent, showClose, showSecondary, primaryAction, secondaryAction ) => {
        // Generate the template
        let modalOutput = template({
            ShowClose: showClose,
            ShowSecondary: showSecondary,
            ModalTitle: title,
            BodyContent: bodyContent,
            PrimaryButton: primaryAction,
            SecondaryButton: secondaryAction
        });

        // Append the modal to the app root element
        App.appRoot.insertAdjacentHTML('beforeend', modalOutput);

        // Display the modal
        document.querySelector('.modal').classList.add('is-active');

        // Animate the opening
        anime({
            targets: '#modal-ui',
            opacity: 0,
            keyframes: [
                { opacity: 0, duration: 0 },
                { opacity: 1, duration: 3000 }
            ]
        });
    },

    /**
     * Hide and remove the modal
     */
    removeModal: () => {
        // Animate the removal of the modal
        anime({
            targets: '.modal',
            keyframes: [
                { opacity: 1, duration: 0 },
                { opacity: 0, duration: 500 }
            ],
            complete: () => {
                document.querySelector('.modal').remove();
            }
        })
    },

    /**
     * Attaches the default action of closing a modal
     */
    setPrimaryAsClose: () => {
        document.querySelector('#modal-ui-primary-button').addEventListener('click', () => {
            Modal.removeModal();
        });
    },

    /**
     * Hides / removes the primary modal action (for situations where we can't let the user continue)
     */
    removePrimaryAction: () => {
        document.querySelector('#modal-ui-primary-button').remove();
    }
};

// Exports ------------------------------------------------------------------ /
export { Modal };