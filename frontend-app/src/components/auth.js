/**
 * auth.js
 * 
 * The authentication system for user / staff accounts in the Global One 
 * Restaurant System. Created for GRDE3014, Web Authoring Design,
 * Assignment 3.
 * 
 * Inspired by GRDE3014, Web Authoring Design, Weekly workshop content by
 * Daniel Brouse, but customised heavily to work with my backend which
 * uses a passport based jwt authentication system.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 1.0.3
 */

// Imports ------------------------------------------------------------------ /
import { App } from './app.js';                     // The core SPA system component
import { Modal } from './modal.js';                 // The modal component
import { Account } from './account.js';             // The accounts system component

// Declaration -------------------------------------------------------------- /
const Auth = {
    // Object Properties
    token: "",
    authenticated: false,
    // Expiry and generation time not required as server will reject when expired anyhow
    // expiry: "",
    // generated: "",

    /**
     * Initialise the authorisation object
     */
    init: () => {
        // For debugging
        console.log(`[Auth] Initialised`);

        // TODO FUTURE - Do initialisation stuff here
    },

    /**
     * Attempts to authenticate the user on the system
     * @return {Boolean} True if successful, false otherwise
     */
    authenticate: (userData) => {        
        // Do a fetch to the backend-api URI
        let result = fetch('http://localhost:8081/api/auth', {
            method: 'post',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
            },
            body: userData
        })
        .then( (res) => {
            if( res.status != 200 ) {
                // If we didn't get the HTTP Status OK from the server

                // For debugging
                // console.log( `Problem signing in - Status ${res.status} given from server` );
                // console.log( res.json() );

                return res.status;
            } else {
                // Looks like it worked? Let's continue
                res.json().then( (jsonRes) => {
                    // For debugging
                    // console.log(jsonRes);
                    // console.log(jsonRes.token);
                    // console.log(jsonRes.expires);

                    // Setup our Auth object parameters
                    Auth.token = jsonRes.token;
                    Auth.authenticated = true;

                    // Get our account information populated
                    Account.init( jsonRes.user );

                    // Expiry and generation time not required ad server will reject when expired anyhow
                    // Auth.expiry = jsonRes.expires;
                    // Auth.generated = Date.now();
                });
                // Return true to the caller
                return res.status;
            }
        })
        .catch( (err) => {
            // A problem occured when trying to authenticate
            console.log(err);
            return err;
        });
        return result;
    },

    /**
     * Small helper to clear the the auth object
     */
    deauthenticate: () => {
        Auth.token = "",
        Auth.authenticated = false;
    },

    /**
     * Sign out of the system
     */
    showSignoutConfirmation: () => {        
        // For debugging
        // console.log('[Auth] Logout button clicked!');

        // Show the confirmation modal
        Modal.displayConfirmation(
            'Confirmation',
            'Are you sure you want to logout?',
            'Yes',
            'No'
        );

        // Assign event listeners to the buttons
        
        // Cancel logout
        document.querySelector('#modal-ui-secondary-button').addEventListener('click', () => {
            Modal.removeModal();
        });

        // Do the logout
        document.querySelector('#modal-ui-primary-button').addEventListener('click', () => {
            // Close the modal
            if( location.hash == '' || location.hash == '#' ) {
                // If we are in the landing page, destroy the login modal, otherwise, not required as innerHTML is destroyed
                Modal.removeModal();
            }
            // De-authenticate
            Auth.deauthenticate();
            // Update the navigation bar to reflect the changes
            App.initNav();
            App.updateNav();
            // Take the user to the landing page by changing the hash value
            location.hash = '';
        });
    }
};

// Exports ------------------------------------------------------------------ /
export { Auth };