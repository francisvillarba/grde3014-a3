/**
 * index.js
 * 
 * The primary javascript file for GRDE3014, Web Authoring Design
 * Assignment 3's Index Page.
 * 
 * This requires the use of webpack to function correctly.
 * DO NOT RUN FROM ./src/ directly, only run from the ./dist/
 * 
 * See README for more information
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 * @version 3.0.0
 */

// Imports ------------------------------------------------------------------ /

// Core components
import './scss/master.scss';                        // Master SCSS Stylesheet (for webpack)
import { App } from './components/app.js';          // The core SPA app script

// Page Controllers
import { landingPageController } from './controllers/landing.js';               // The Landing / Home Page
import { constructionPageController } from './controllers/construction.js';     // Under Construction Page
import { loginPageController } from './controllers/login.js';                   // Login Page
import { orderPageController } from './controllers/order.js';                   // Order Page
import { profilePageController } from './controllers/profile.js';               // Profile Page
import { registrationPageController } from './controllers/register.js';         // Register Page
import { menuPageController } from './controllers/menu.js';                     // Menu Page
import { itemManagementPageController } from './controllers/manage-items.js';   // Item Management Feature


// Initialisation ----------------------------------------------------------- /

// Declare Routes
App.addRoute( '#', landingPageController );                     // Landing Page
App.addRoute( '#login', loginPageController );                  // Login Page
App.addRoute( '#order', orderPageController );                  // Order Page
App.addRoute( '#profile', profilePageController );              // Profile Page
App.addRoute( '#register', registrationPageController );        // Register Page
App.addRoute( '#menu', menuPageController );                    // Menu Page
App.addRoute( '#manage-items', itemManagementPageController );  // Item Management Page

// Declare under construction routes
App.addRoute('#about', constructionPageController);         // About Page
App.addRoute('#gallery', constructionPageController);       // Gallery Page
App.addRoute('#contact', constructionPageController);       // Contact Us Page

// Init the core app component when the DOM is loaded and ready for action
document.addEventListener( 'DOMContentLoaded', App.init );